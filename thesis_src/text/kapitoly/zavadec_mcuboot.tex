\chapter{Zavaděč MCUboot}
MCUboot je bezpečný zavaděč pro 32-bitové mikroprocesory. MCUboot nezávisí na žádném konkrétním operačním systému nebo hardwaru. Většina funkcionality je systémově nezávislá, systémově závislá funkcionalita (čtení flash, způsob zavádění firmware) je vždy implementována pro specifický operační systém, architekturu nebo \acrshort{soc}. Zavaděč umožňuje aktualizace firmwaru, recovery po sériové lince a je odolný vůči chybám během aktualizace.
\cite{mcuboot}

Zavaděč podporuje secure boot. MCUboot neimplementuje vlastní kryptografickou knihovnu, ale spoléhá se na Mbed TLS, případně TinyCrypt. 
Pokud není žádoucí použití těchto knihoven, je možné implementovat své vlastní kryptografické funkce. 
Na většině moderních SoC jsou hardwarově implementované kryptografické akcelerátory. MCUboot umožňuje využití těchto akcelerátorů, uživatel může implementovat kryptografické funkce využívající hardwarové akcelerace. Takto používá MCUboot na svých produktech firma Nordic Semiconductor, kde jsou kryptografické funkce implementované s pomocí hardwarového akcelerátoru ARM CryptoCell. Díky tomu je ověřování firmware rychlejší a v zavaděči nemusí být zakompilovány externí knihovny, což ušetří paměť \acrshort{ram} i flash.
\cite{mcuboot} \cite{nrf_connect}

Projekt MCUboot se skládá ze dvou hlavních částí, knihovny bootutil a samotné aplikace zavaděče.
Knihovna bootutil implementuje většinu funkcionality zavaděče, včetně secure boot, podpory pro aktualizace firmware a recovery. Tato část je systémově nezávislá. Samotná aplikace zavaděče využívá zmíněnou knihovnu a implementuje poslední krok nutný k nastartování aplikace, zavádění a skok do aplikace. Tato část je závislá na architektuře systému, na různých architekturách má firmware jiný formát a tudíž je postup jiný. 
\cite{mcuboot} \cite{mcuboot_memfault}

\section{Rozdělení flash paměti}

Z pohledu zavaděče MCUboot je flash paměť rozdělena do několika segmentů. V této práci jsou segmenty označovány jako sloty.
Každá aplikace má ve flash paměti vyhrazené dva sloty, primární a sekundární. Standardně zavaděč zavádí aplikaci z primárního slotu. Aplikace je sestavená pro běh právě z tohoto primárního slotu. Sekundární slot slouží jako místo pro umístění nové verze. Rozdělení flash paměti je definované v hlavičkovém souboru \lstinline{sysflash.h} pomocí maker.
\cite{mcuboot}

Při spuštěni zavaděč kontroluje, zda se v sekundárním slotu nenachází připravená aktualizace. Pokud je k dispozici aktualizace, zavaděč vymění obsah primárního a sekundárního slotu. V závislosti na konfiguraci k tomu může využít odkládací prostor. Výjimkou jsou speciální režimy aktualizace Direct-XIP a RAM-load, kdy může být aplikace zavedena z obou slotů. 
\cite{mcuboot}

V dnešní době stále více \acrshort{soc} obsahuje více-jádrové procesory, které umožňuji \acrshort{amp} výpočet. Společně s \acrshort{amp} je také nutné distribuovat rozdílné aplikace pro jednotlivá jádra. Zavaděč MCUboot podporuje aktualizace více aplikací. Každá aplikace má svůj vlastní primární a sekundární slot. Zavádění všech aplikací již není starost zavaděče MCUboot. Zavaděč provádí pouze zavedení hlavní aplikace, ta má následně na starosti zavedení ostatních aplikací pro další jádra. MCUboot také dokáže vyhodnotit závislosti mezi jednotlivými aplikacemi.
\cite{mcuboot}. Na obrázku \ref{fig:mcuboot_single_layout} je zobrazeno možné rozdělení flash paměti pro jednu aktualizovatelnou aplikaci, na obrázku \ref{fig:mcuboot_dual_layout} rozdělení pro dvě aplikace.
\cite{mcuboot}


\begin{figure}[h!]
    \centering
    \includesvg[width=0.35\textwidth]{text/grafika/mcuboot_flash_layout.svg}
    \caption{Příklad rozdělení flash paměti pro jednu aplikaci}
    \label{fig:mcuboot_single_layout}
\end{figure}


\begin{figure}[h!]
    \centering
    \includesvg[width=0.35\textwidth]{text/grafika/mcuboot_multi_image_layout.svg}
    \caption{Příklad rozdělení flash paměti pro dvě aplikace}
    \label{fig:mcuboot_dual_layout}
\end{figure}




\section{Aktualizace firmware}
Pokud je zařízení zapnuto, je spuštěn zavaděč MCUboot. 
Za normálních okolností je v primárním slotu nejnovější aplikace, a není tedy potřeba provádět výměnu slotů. 
Během aktualizace aplikace je do sekundárního slotu nahrána nová verze a zavaděč má za úkol provést výměnu slotů. Po spuštění se vždy vyhodnotí jakou operaci má provést pomocí aktuálního stavu výměny.
\cite{mcuboot}

Aktualizace může být dvou-krokový proces. 
Jako první krok MCUboot provede testovací výměnu slotů tím, že nastaví stav výměny na testovací a spustí novou aplikaci.
Aplikace se následně může sama otestovat a poté nastaví stav výměny jako permanentní. 
Pokud by stav zůstal nastavený na testovací, při dalším spuštění zavaděč provede reverzní výměnu a vrátí tak zpět původní verzi aplikace. Již během nahrávání nové aplikace do sekundárního slotu má uživatel možnost nastavit stav výměny. Pokud uživatel nastaví permanentní stav, aplikace již nemusí nic potvrzovat, aktualizace zůstane permanentní.
\cite{mcuboot}

Stav výměny je uložen společně s dalšími metadaty v posledním sektoru slotu. Stav výměny slotů může nabývat následujících stavů:
\begin{description}[style=nextline]
    \item[Prázdný stav] 
    Standardní stav, pokud není prováděná aktualizace. Výměna slotů není provedena.
    
    \item[Testovací stav]
    Vymění primární a sekundární slot. V případě že není stav nastaven na permanentní je při dalším spuštění stav nastaven na reverzní.
    
    \item[Permanentní stav]
    Provede se permanentní výměna slotů.

    \item[Reverzní stav]
    Předchozí testovací výměna neproběhla úspěšně. Zavaděč vymění obsah slotů zpět do původního stavu.
    
    \item[Chybový stav]
    Výměna se nezdařila, protože obsah sekundární slotu není validní.

    \item[Stav panika]
    Během výměny došlo k chybě, ze které se nelze zotavit. \cite{mcuboot}
\end{description}

\subsection{Způsob výměny slotů}

MCUboot podporuje několik způsobů, jakým provádí aktualizaci. Během každého pracuje s primárním a sekundárním slotem. Způsoby aktualizace jsou následující: 
\begin{description} [style=nextline]
    \item[Výměna pomocí odkládacího prostoru] 
    Výměna pomocí tohoto způsobu využívá vyhrazený odkládací prostor ve flash paměti. Velikost odkládacího prostoru musí být větší nebo stejná jako velikost jedné stránky. Opotřebení flash paměti závisí na velikosti odkládací prostoru. Použití většího prostoru snižuje opotřebení flash paměti.
    
    \item[Výměna bez odkládacího prostoru]
    Výměna je také možná bez použití odkládacího prostoru. Postupně jsou sektory posouvány dokud nejsou obsahy slotů vyměněny. Algoritmus funguje následovně:
    \begin{enumerate}
        \item Všechny sektory v primárním slotu posuň o jeden výše. Nastav N=0.
        \item Zkopíruj N-tý sektor sekundárního slotu do N-tého sektoru primárního slotu.
        \item Zkopíruj N+1. sektor primárního slotu do N-tého sektoru sekundárního slotu.
        \item Opakuj kroky 2 a 3 pro všechny sektory.
    \end{enumerate}
    Výhoda tohoto způsobu je úspora flash paměti, bohužel za cenu většího opotřebení flash paměti.
    
    \item[Direct-XIP]
    V tomto režimu nedochází k výměně obsahu slotů, aplikace může být zavedena také přímo ze sekundárního slotu. Zavaděč provede inspekci primárního a sekundární slotu a na základě verze se rozhodne, kterou aplikaci zavede. Zaváděna je aplikace, která má vyšší verzi. 
    V tomto režimu je také umožněno testovat aktualizaci aplikace. Místo provedení reverzní výměny je aplikace ze slotu odstraněna.
    Výhodou tohoto režimu je rychlejší průběh aktualizace a nižší opotřebení flash paměti. Velkou nevýhodou je potřeba sestavovat dvě verze aplikace, jednu pro primární slot a druhou pro sekundární.

    \item[Zavedení do RAM]
    Funguje téměř stejně jako režim Direct-XIP, rozdíl je v tom, že aplikace běží čistě v paměti RAM. \cite{mcuboot}
\end{description}


\subsection{Metadata}
Na konci slotů je vyhrazené místo pro metadata. Pomocí nich zavaděč pozná aktuální stav a jakou operaci má během svého běhu provést. Metadata jsou uložena v obou slotech, \mbox{primárním i} sekundárním, a obsahují následující položky:
\begin{description} [style=nextline]
    \item[Magic]
    Slovo určující přítomnost metadat.
    \item[Swap status]
    Během výměny slotů provádí zavaděč různé operace se sektory. Zde je uložena operace, jaká se zrovna provádí. Pokud by došlo k výpadku napájení během aktualizace, zavaděč je díky těmto informacím schopný pokračovat ve výměně slotů.
    \item[Swap size]
    Počet sektorů, které je nutné vyměnit. Zpravidla se rovná velikosti větší aplikace v primárním nebo sekundárním slotu.
    \item[Swap info]
    Informace o tom jaká operace výměny slotů je právě prováděna (testovací, permanentní, reverzní).
    \item[Copy done]
    Hodnota indikující, že se aplikace nachází v cílovém slotu.
    \item[Image OK]
    Uživatelsky zapsaná hodnota, indikující, že je aplikace připravena k aktualizaci. \cite{mcuboot}
\end{description}

MCUboot se podle informací v metadatech rozhodne jakou operaci provede. Zda-li došlo v předešlém běhu k přerušení aktualizace zavaděč pozná pomocí položek \lstinline{swap info} a \lstinline{swap status}. Následně pokračuje od bodu kde byla operace přerušena.
\cite{mcuboot}

Během nové operace výměny slotů (například po nahrání aktualizace do sekundárního slotu) nejsou položky \lstinline{swap info} a \lstinline{swap status} nastaveny. Zavaděč zjistí aktuální stav metadat primárního i sekundárního slotu a na základě položek \lstinline{magic}, \lstinline{image ok} a \lstinline{copy done} se rozhodne jakou operaci provede. V tabulkách níže je uvedeno jak dojde k vyhodnocení výsledné operace.
\cite{mcuboot}

\begin{table}[h!]
\centering
\caption{\label{tab:mcuboot_swap_result} MCUboot - vyhodnocení operace I \cite{mcuboot}}
    \begin{tabular}{ | l | l | l |}
      \hline
      \textbf{} & \textbf{primární slot} & \textbf{sekundární slot} \\
      \hline
      \hline
      magic & nezáleží & v pořádku \\
      \hline
      image ok & nezáleží & nenastaven \\
      \hline
      copy done & nezáleží & nezáleží \\
      \hline
      \textbf{výsledná operace} & \multicolumn{2}{|l|}{\textbf{testovací výměna}} \\
      \hline
  \end{tabular}
\end{table}

\begin{table}[h!]
\centering
\caption{\label{tab:mcuboot_swap_result_1} MCUboot - vyhodnocení operace II \cite{mcuboot}}
    \begin{tabular}{ | l | l | l |}
      \hline
      \textbf{} & \textbf{primární slot} & \textbf{sekundární slot} \\
      \hline
      \hline
      magic & nezáleží & v pořádku \\
      \hline
      image ok & nezáleží & nastaven \\
      \hline
      copy done & nezáleží & nezáleží \\
      \hline
      \textbf{výsledná operace} & \multicolumn{2}{|l|}{\textbf{permanentní výměna}} \\
      \hline
  \end{tabular}
\end{table}

\begin{table}[h!]
\centering
\caption{\label{tab:mcuboot_swap_result_2} MCUboot - vyhodnocení operace III \cite{mcuboot}}
    \begin{tabular}{ | l | l | l |}
      \hline
      \textbf{} & \textbf{primární slot} & \textbf{sekundární slot} \\
      \hline
      \hline
      magic & v pořádku & nenastaven \\
      \hline
      image ok & nenastaven & nezáleží \\
      \hline
      copy done & nastaven & nezáleží \\
      \hline
      \textbf{výsledná operace} & \multicolumn{2}{|l|}{\textbf{reverzní výměna}} \\
      \hline
  \end{tabular}
\end{table}

\pagebreak

\subsection{Kroky aktualizace aplikace}

Níže jsou uvedeny kroky během aktualizace aplikace od spuštění zavaděče MCUboot až po zavedení aplikace: 
\begin{enumerate}
   \item Zkontroluj stav výměny slotů, byla předchozí operace přerušena?
   \begin{itemize}
     \item Ano: Dokonči výměnu slotů.
     \item Ne: pokračuj na krok 2.
   \end{itemize}
   \item Zkontroluj metadata, je vyžadována výměna slotů?
   \begin{itemize}
     \item Ano: Proveď výměnu slotů, pokud je aplikace v sekundárním slotu úspěšně ověřena, jinak aplikaci odstraň a pokračuj na krok 3.
     \item Ne: Pokračuj na krok 3.
   \end{itemize}
   \item Ověř integritu, případně důvěryhodnost primárního slotu a spusť aplikaci.
\end{enumerate}
V případě, že je MCUboot nakonfigurován pro podporou více aplikací, jsou výše zmíněné kroky provedeny pro každou aplikaci. Navíc je ještě kontrolována závislost mezi aplikacemi.
\cite{mcuboot}


\section{Formát obrazu}
\label{section:mcuboot_image_format}

Aby byl zavaděč schopný pracovat s firmwarem, je nutné aby jeho obraz měl předem definovaný formát. MCUboot definuje strukturu obrazu. Zjednodušeně řečeno, firmware je obalen hlavičkou a TLV záznamy. Struktura je znázorněna na obrázku \ref{fig:mcuboot_image_format}.
Společně se zavaděčem MCUboot je distribuován také nástroj \lstinline{imgtool}, který z firmwaru vytvoří obraz ve zmíněném formátu. 
\cite{mcuboot}

\begin{figure}[h!]
    \centering
    \includesvg[width=0.65\textwidth]{text/grafika/mcuboot_image_format.svg}
    \caption{MCUboot - formát obrazu firmwaru \cite{mcuboot}}
    \label{fig:mcuboot_image_format}
\end{figure}

\pagebreak

\subsection{Hlavička}

Hlavička má velikost 32 bytů. Na jejím začátku se nachází magické slovo, podle kterého zavaděč pozná, že se v paměti nachází aplikace ve správném formátu. Dále je zde přítomná také velikost hlavičky (vyhrazené místo pro MCUboot hlavičku může být větší než 32 bytů), velikost firmwaru, verze firmwaru a další.
Verze obrazu má tvar \lstinline{<major>.<minor>.<revision>+<build_number>}. Kompletní struktura je vyobrazená na výpisu \ref{lst:mcuboot_hdr}.
\cite{mcuboot}

\begin{lstlisting}[caption={MCUboot - struktura hlavičky \cite{mcuboot}}, label={lst:mcuboot_hdr}]
#define IMAGE_MAGIC                 0x96f3b83d
#define IMAGE_HEADER_SIZE           32

struct image_version {
    uint8_t iv_major;
    uint8_t iv_minor;
    uint16_t iv_revision;
    uint32_t iv_build_num;
};

/** Image header.  All fields are in little endian byte order */
struct image_header {
    uint32_t ih_magic;
    uint32_t ih_load_addr;
    uint16_t ih_hdr_size;
    uint16_t ih_protect_tlv_size;
    uint32_t ih_img_size;
    uint32_t ih_flags;
    struct image_version ih_ver;
    uint32_t _pad1;
};
\end{lstlisting}


\subsection{TLV záznamy}

Na konci firmwaru se nacházejí \acrshort{tlv} (type-length-value) záznamy. Tyto záznamy udávají další informace o firmwaru, například jak je firmware podepsán nebo šifrován. Také je zde uložen hash pro ověření integrity a pokud je používán secure boot nachází se zde také podpis. \cite{mcuboot}

Rozlišují se dva typy \acrshort{tlv} záznamů, obyčejné a chráněné. V chráněných \acrshort{tlv} je typicky uložený podpis. U chráněných \acrshort{tlv} je oproti obyčejným navíc ověřována integrita. 
Struktura TLV záznamu je vyobrazena na výpisu \ref{lst:mcuboot_tlv}.\cite{mcuboot}

\pagebreak

\begin{lstlisting}[caption={MCUboot - struktura TLV záznamu \cite{mcuboot}}, label={lst:mcuboot_tlv}]
#define IMAGE_TLV_INFO_MAGIC        0x6907
#define IMAGE_TLV_PROT_INFO_MAGIC   0x6908

/** Image TLV header.  All fields in little endian. */
struct image_tlv_info {
    uint16_t it_magic;
    uint16_t it_tlv_tot; /* size of TLV area */
};

/** Image trailer TLV format. All fields in little endian */
struct image_tlv {
    uint8_t  it_type;
    uint8_t  _pad;
    uint16_t it_len;
};
\end{lstlisting}

\section{Nástroj imgtool}

V repositáři projektu MCUboot se společně se zdrojovými kódy zavaděče nachází také nástroj \lstinline{imgtool}. Nástroj slouží ke správě klíčů, podepisování firmware a vytváření obrazu ve formátu kompatibilním s MCUboot.
\cite{mcuboot}

Příkazem \lstinline{imgtool.py keygen} je možné vygenerovat privátní klíč, pomocí kterého bude aplikace následně podepsána. MCUboot podporuje klíče \acrshort{rsa}, \acrshort{ecdsa} a \acrshort{eddsa}. Příkaz \break \lstinline{imgtool.py getpub} extrahuje z privátního klíče veřejný klíč, výstupem je soubor v jazyce C, který obsahuje binární reprezentaci veřejného klíče. Veřejný klíč je následně zakompilován do firmwaru zavaděče. Všechny aplikace, se kterými bude v budoucnu zavaděč pracovat je následně nutné podepsat odpovídajícím privátním klíčem. Pomocí příkazu \lstinline{imgtool.py sign} je možné vytvářet obraz firmwaru v předepsaném formátu. Výsledný soubor vždy obsahuje SHA256 hash, volitelně je také možné aplikaci podepsat. Privátní klíč, pomocí kterého se aplikace podepíše se specifikuje parametrem \lstinline{--key}. Závislosti mezi aplikacemi mohou být specifikovány pomocí argumentu \lstinline{-d "(image_id, image_version)"}, kde \lstinline{image_id} je číslo aplikace, na kterém aktuální aplikace závisí, \lstinline{image_version} je pak minimální potřebná verze specifikované aplikace. Například argument \lstinline{-d "(1, 1.2.3+0)"} specifikuje závislost na aplikaci ve slotu číslo 1, minimální požadovaná verze je \lstinline{1.2.3+0}.
\cite{mcuboot}

\section{Port pro ESP32}

MCUboot umí pracovat pouze s obrazem firmwaru, který je ve specifickém formátu popsaném v sekci \ref{section:mcuboot_image_format}.
Formát firmwaru aplikace při použití zavaděče MCUboot se liší od standardního formátu pro platformu ESP32 popsaného v sekci \ref{section:esp32_image_format}. Struktura obrazu firmwaru je uvedena na obrázku \ref{fig:mcuboot_esp32_image_format}. 

\begin{figure}[h!]
    \centering
    \includesvg[width=0.8\textwidth]{text/grafika/esp32_mcuboot_image.svg}
    \caption{MCUboot - struktura obrazu firmware aplikace pro ESP32 \cite{mcuboot}}
    \label{fig:mcuboot_esp32_image_format}
\end{figure}

Na začátku firmware se nacházejí metadata, která obsahují data potřebná k nahrání aplikace do paměti a její spuštění. Struktura metadat je vyobrazena v tabulce \ref{tab:mcuboot_esp32_metadata}.  Po metadatech následují data a instrukce aplikace, které se do paměti nahrávají případně mapují do adresního prostoru. Během linkování aplikace se počítá s tím, že prvních 32 bytů zabírá hlavička pro zavaděč MCUboot. Výsledný firmware má na začátku 32 bytovou výplň, která se během vytváření obrazu nástrojem \lstinline{imgtool} nahradí hlavičkou.
\cite{mcuboot}


\begin{table}[h!]
\centering
\caption{\label{tab:mcuboot_esp32_metadata} Struktura metadat \cite{mcuboot}}
    \begin{tabular}{ | l | l | l |}
      \hline
      \textbf{offset} & \textbf{velikost (bytů)} & \textbf{popis} \\
      \hline
      \hline
      0 & 4 & Magické slovo \\
      \hline
      4 & 4 & Adresa vstupního bodu do aplikace\\
      \hline
      8 & 4 & Adresa IRAM regionu v paměti RAM\\
      \hline
      12 & 4 & Adresa IRAM regionu ve flash paměti relativní vůči slotu \\
      \hline
      16 & 4 & Velikost IRAM regionu \\
      \hline
      20 & 4 & Adresa DRAM regionu v paměti RAM  \\
      \hline
      24 & 4 & Adresa IRAM regionu ve flash paměti relativní vůči slotu  \\
      \hline
      28 & 4 & Velikost DRAM regionu \\
      \hline
    \end{tabular}
\end{table}

Po startu čipu je automaticky spuštěn zavaděč v paměti \acrshort{rom}. Tento zavaděč využívá část \acrshort{ram} paměti pro zásobník a pro data patřící do sekcí .bss (data inicializovaná na nulu) a .data (inicializovaná data). Zavaděč si během nahrávání budoucího firmware nesmí přepsat svou vlastní paměť.
Paměť zavaděče MCUboot, který je využíván jako další zavaděč, proto nesmí kolidovat s pamětí \acrshort{rom} zavaděče. Stejně tak během zavádění aplikace nesmí MCUboot přepsat svou vlastní paměť. Rozdělení paměti je zobrazeno na obrázku \ref{fig:esp32_mcuboot_mem_layout}. Některé čipy ESP32, například ESP32-S3 obsahují dvou-jádrový procesor. V takovém případě jsou v paměti vyhrazené oblasti pro zásobníky obou jader.
\cite{esp_idf_guide} \cite{mcuboot}
\begin{figure}[h!]
    \centering
    \includesvg[width=0.35\textwidth]{text/grafika/mcuboot_memory_layout.svg}
    \caption{MCUboot - rozdělení paměti RAM \cite{mcuboot}}
    \label{fig:esp32_mcuboot_mem_layout}
\end{figure}

Samotný zavaděč MCUboot je ještě rozdělen do tří paměťových regionů: \lstinline{iram}, \lstinline{iram_loader} a \lstinline{dram}. Oproti standardní aplikaci jsou instrukce rozděleny do dvou regionů. Region \lstinline{iram_loader} obsahuje pouze zdrojový kód pro nahrání aplikace do paměti a její spuštění. Ostatní instrukce (logika aktualizací firmware, kryptografické knihovny, recovery, ...) jsou v regionu \lstinline{iram}. Díky tomuto rozdělení je při nahrávání aplikace možné přepsat část zavaděče MCUboot, konkrétně region \lstinline{iram}. Díky tomu má aplikace k dispozici více paměti pro statická data (ve výchozí konfiguraci jde o 32 KB). Po spuštění aplikace již paměť zavaděče MCUboot není využívaná a je možné jí celou využít pro haldu. 
\cite{esp_idf_guide} \cite{mcuboot} 