\chapter{Operační systém Zephyr}

Zephyr je open-source operační systém reálného času speciálně navržený pro využití ve vestavných systémech s omezenými zdroji a \acrshort{iot} zařízeních. Je vyvíjen jako open-source projekt pod záštitou Linux Foundation. Zephyr poskytuje robustní a flexibilní prostředí pro vývoj vestavěných systémů s různými požadavky na výkon, spotřebu energie a paměť. Operační systém podporuje širokou škálu architektur (ARM, RISC-V, Xtensa, MIPS, ...). Velkou předností systému je veliká modularita a hardwarová abstrakce. Pro začátečníky toto může zpočátku vypadat jako nevýhoda, ale pokud je aplikace správně napsaná, je velice jednoduché ji portovat na jinou architekturu.
\cite{zephyr}



Na rozdíl od, dnes také velmi populárního systému, FreeRTOS není Zephyr jen plánovač, ale jde o celý ekosystém. Součástí je řada nástrojů, které usnadňují vývoj a testování. Aplikace je možné spouštět v QEMU, případně jako nativní aplikaci v operačním systému Linux. Dále systém obsahuje například terminál pro komunikaci se zařízením pomocí sériové linky a implementuje také několik způsobu pro aktualizace firmware. Systém nabízí bohatou podporu pro síťové připojení, obsahuje implementaci pro komunikaci pomocí \acrshort{tcp}/\acrshort{ip} a vlastní implementaci bluetooth kontroléru. Některé principy jsou převzaté z operačního systému Linux. Konkrétně definice hardwaru pomocí devicetree a konfigurace jádra pomocí Kconfig. Operační systém Zephyr kompatibilní s rozhraním \acrshort{posix}.
\cite{zephyr} \cite{devicetree_linux} \cite{kconfig}

\section{Jádro}

Jádro je srdce každé aplikace využívající operační systém Zephyr. Poskytuje prostředí s podporou vícevláknového zpracování procesů a bohatou sadu dostupné funkcionality. Díky konfigurovatelnému jádru je možné do aplikace začlenit pouze potřebnou funkcionalitu a je tak ideální pro aplikace s omezenými zdroji. Při minimální konfiguraci jádra je požadavek na velikost paměti pouze 2 KB.
\cite{zephyr}


\subsection{Plánovač}
Systém vybírá aktuálně spuštěné vlákno na základě jeho priority. Vlákno s nejvyšší prioritou má přednost. Pokud existuje více vláken se stejnou prioritou, systém vybere takové, které na zahájení čeká delší dobu. Priorita je celé číslo. Vlákno, které má nastavené nižší číslo priority má přednost před vlákny s vyšším číslem priority.
\cite{zephyr}

\subsection{Vlákna}
Vlákno je nejmenší spustitelná jednotka operačního systému. Systém rozlišuje mezi dvěma typy vláken, kooperativním a preemptivním.
\begin{description} [style=nextline]
    \item[Kooperativní vlákno]
    Vlákno které je označeno jako kooperativní nemůže být plánovačem přerušeno, pokud k tomu samo nevykoná nějakou akci. Akcí vedoucí k přerušení může být například uspání vlákna, čekání na semafor nebo zavolání \lstinline{k_yield()}.
    
    \item[Preemptivní vlákno]
    Vlákno označené jako preemptivní může být během svého běhu přerušené. Nejčastěji se tak děje pokud je připravené jiné vlákno s vyšší prioritou.
\end{description}
Každé vlákno může nabývat následujících třech stavů: běžící, připravené, neaktivní. Neaktivní je vlákno, které z nějakého důvodu nemůže běžet, například čeká na dokončení nějaké operace, bylo uspáno nebo ukončeno. Pokud chce být neaktivní vlákno spuštěno musí nejprve přejít do stavu připravené. V tomto stavu čeká, než ho plánovač vybere jako další aktivní vlákno. Když plánovač vlákno zvolí ke spuštění, je jeho stav nastaven na bežící.
\cite{zephyr}

Systém dále dělí vlákna na uživatelská a systémová. Uživatelské je takové, které vytvořil uživatel, zatímco systémové je automaticky vytvořeno při startu systému. Systémová vlákna jsou minimálně dvě, vlákno \lstinline{main} a \lstinline{idle}.
\cite{zephyr}

Vlákno \lstinline{main} provádí nezbytnou inicializaci operační systému a spouští funkci \lstinline{main()}. Po skončení funkce \lstinline{main()} je vlákno ukončeno, systém ale stále běží. Vlákno \lstinline{idle} je aktivní pouze pokud není připravené žádné jiné vlákno. Pokud to daná platforma podporuje, může být  toto vlákno použito k přepnutí hardwaru do úsporného režimu. 
\cite{zephyr}

\subsection{Pracovní fronta}
Pracovní fronta je funkcionalita jádra, která využívá dedikované vlákno na zpracování pracovního objektu. Pracovní objekty jsou zpracovávány v pořadí v jaké byly do pracovní fronty zařazeny. Každý objekt pracovní fronty je zpracován tím, že je vykonána funkce, která příslušnému objektu náleží. Typicky je pracovní fronta využívána při obsluze přerušení nebo vláknem s vysokou prioritou pro odložené zpracování. Vlákno nebo obsluha přerušení vytvoří pracovní objekt a odešle ho do pracovní fronty. Pracovní objekt je následně zpracován na vlákně s nižší prioritou, až na něj přijde řada.
\cite{zephyr}

\subsection{Obsluha přerušení}
Obsluha přerušení je funkce, která se spustí asynchronně na základě hardwarového nebo softwarového přerušení. Obsluha přerušení není plánována, aktuálně vykonávané vlákno je přerušeno. Díky tomu je zajištěna minimální režie během odezvy na přerušení. Vlákno je obnoveno po obsloužení všech přerušení. 
\cite{zephyr}

Jádro podporuje také vnořená přerušení. Tím je umožněno, aby byla aktuálně vykonávaná obsluha přerušena jiným přerušením s vyšší prioritou. Obsluha přerušení s nižší prioritou je obnovena poté, co je dokončena obsluha přerušení s vyšší prioritou.
\cite{zephyr}

\subsection{Uživatelský prostor}
Jádro operačního systém Zephyr má také podporu pro uživatelský prostor. Tím je možné oddělit část paměti, kterou využívá jádro systému od paměti, kterou využívá aplikace. Hlavním cílem uživatelského režimu je poskytovat ochranu paměti a ochranu hardwaru před škodlivým nebo chybným chováním softwaru. Pro podporu je požadováno aby cílová platforma měla implementované \acrshort{mpu}. Platforma ESP32 obsahuje hardwarovou jednotku pro ochranu paměti, avšak tato funkcionalita zatím není v operačním systému Zephyr podporována.
\cite{zephyr}

\subsection{Ovladače}

Standardní cestou, jak přistupovat k periferiím v operačním systému Zephyr je prostřednictvím ovladačů. Pro každý typ ovladače (\acrshort{uart}, \acrshort{spi}, \acrshort{i2c}, watchdog, časovač, ...) je v systému obecné rozhraní. S pomocí této abstrakce je možné psát aplikace nezávisle na platformě, na které budou spuštěny. Pro přístup k hardwaru aplikace se vyžívá jednotné rozhraní, které je identické napříč platformami.
\cite{zephyr}

Každý ovladač je reprezentovaný strukturou \lstinline{device}. Struktura obsahuje název ovládače (\lstinline{name}), pointer na strukturu s konfigurací ovladače (\lstinline{config}), pointer na data ovladače  \lstinline{data} a pointer na strukturu, která mapuje obecné rozhraní na konkrétní implementaci (\lstinline{api}).
\cite{zephyr}

\begin{lstlisting}[caption={Objekt reprezentující ovladač \cite{zephyr}}]
struct device {
      const char *name;
      const void *config;
      const void *api;
      void * const data;
};
\end{lstlisting}

Struktura \lstinline{device} je vytvořena během inicializace ovladače. Pořadí v jakém jsou ovladače inicializovány je možné ovlivnit nastavením priority. \cite{zephyr}

Následující příklad odešle na sériovou linku znak \lstinline{x}. Díky hardwarové abstrakci pomocí ovladačů bude příklad fungovat na všech platformách podporovaných v operačním systému Zephyr.
\begin{lstlisting}[caption={Příklad použití ovladače pro periferii UART}]
const struct device * dev = device_get_binding("uart0");
uart_poll_out(dev, 'x');
\end{lstlisting}

\section{Kconfig}
Kconfig je systém pro výběr konfigurace, který se běžně používá během sestavování projektu. Umožňuje povolit nebo zakázat funkcionalitu projektu. Původně byl vyvinut pro konfiguraci linuxového jádra, ale nyní se používá i v dalších projektech. \cite{kconfig}

Operační systém Zephyr používá Kconfig pro konfiguraci funkcionality systému. Kconfig dovoluje konfigurovat projekt, aniž by bylo nutné upravovat zdrojový kód. Konfigurační volby (nazývané také symboly) jsou definovány v souborech \lstinline{Kconifg}. V těchto souborech jsou také definované závislosti mezi konfiguračními volbami. Pro větší přehlednost jsou symboly seskupeny do víceúrovňové nabídky. 
\cite{zephyr} \cite{kconfig}
 
Konfiguraci je možné měnit interaktivně pomocí textového rozhraní \lstinline{menuconfig} (obrázek \ref{fig:menuconfig}), případně grafického \lstinline{guiconfig} (obrázek \ref{fig:guiconfig}) nebo je možné konfiguraci definovat přímo v souborech s příponou \lstinline{.conf}. Výstupem konfigurace pomocí Kconfig je hlavičkový soubor \lstinline{autoconf.h}, který obsahuje konfiguraci ve formě maker. Pomocí tohoto souboru je možné přistupovat ke konfiguraci ze zdrojového kódu. Na příkladu \ref{lst:kconfig_def} je definice symbolu \lstinline{FPU}, který přidává podporu pro operace v pohyblivé řádové čárce. Symbol je závislý na symbolu \lstinline{CPU_HAS_FPU}. Symbol \lstinline{FPU} je možné nastavit pouze v případě, že je aktivní také symbol \lstinline{CPU_HAS_FPU}, v opačném případě mu zůstává výchozí hodnota. Symbol je možné nastavit pomocí  \lstinline{menuconfig},  \lstinline{guiconfig} a nebo přidáním řádku \lstinline{CONFIG_FPU=y} do souboru konfigurace projektu \lstinline{prj.conf}.

\pagebreak

\begin{lstlisting}[caption={Kconfig - příklad definice konfiguračních symbolů \cite{zephyr}}, label={lst:kconfig_def}]
config CPU_HAS_FPU
   bool
   help
     This symbol is y if the CPU has a hardware floating point unit.
     
config FPU
   bool "Support floating point operations"
   depends on HAS_FPU
\end{lstlisting}

\begin{figure}[h!]
    \centering
    \includegraphics[width=.80\textwidth]{text/grafika/menuconfig.png}
    \caption{Konfigurace Kconfig pomocí menuconfig \cite{zephyr}}
    \label{fig:menuconfig}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=.80\textwidth]{text/grafika/guiconfig.png}
    \caption{Konfigurace Kconfig pomocí guiconfig \cite{zephyr}}
    \label{fig:guiconfig}
\end{figure}


\section{Devicetree}

V operačním systému Zephyr se devicetree používá k popisu hardwarové konfigurace systému. Jedná se o stromovou datovou strukturu, která definuje zařízení, jejich vlastnosti a jejich vzájemné propojení. Devicetree je deklarativní formát, což znamená, že definuje požadovanou strukturu konfigurace, nikoli to, jak ji dosáhnout. Devicetree umožňuje jednoduchou konfiguraci hardwaru a její přizpůsobení různým platformám, což je zásadní pro přenositelnost a flexibilitu operačního systému. \cite{zephyr} \cite{devicetree} 

Operační systém Zephyr používá devicetree pro popis hardwaru na podporovaných deskách a pro popis počáteční hardwarové konfigurace, Zephyr jej tedy používá jako jazyk pro popis hardwaru a částečně také konfiguraci systému (pomocí devicetree je například nastavena výchozí sériová linka, která se používá pro systémový terminál). \cite{zephyr}

Způsob, jakým operační systém využívá devicetree se značně liší od linuxu. V operačním systému linux jsou zdrojové soubory devicetree převedeny do binární podoby, která je načtena během startu systému. To dovoluje dynamicky měnit konfiguraci hardwaru za běhu systému. Operační systém Zephyr oproti tomu převádí zdrojový kód devicetree do hlavičkového souboru, ve kterém se informace z devicetree nacházejí v podobě maker. Aplikace využívající Zephyr je vždy sestavena pro konkrétní hardwarovou konfiguraci, není ji možné dynamicky měnit za běhu systému. Způsob překladu devicetree je vyobrazen na obrázku \ref{fig:devicetree_build_flow}.
\cite{zephyr} \cite{devicetree_linux}

\begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{text/grafika/deevicetree_build_flow.png}
    \caption{Zpracování devicetree v \acrshort{rtos} Zephyr \cite{zephyr}}
    \label{fig:devicetree_build_flow}
\end{figure}

Samotná konfigurace pomocí devicetree se v \acrshort{rtos} Zephyr skládá ze dvou částí, devicetree bindings a zdrojového souboru.
Devicetree bindings popisují strukturu uzlů, které se používají ve zdrojovém kódu. Jedná se o soubor ve formátu YAML, který definuje jaké má uzel parametry a zda může být vnořený do jiného uzlu. Příklad \ref{lst:devicetree_bindings} popisuje strukturu uzlu definujícího sériové rozhraní. Na dalším příkladu \ref{lst:devicetree_source_example} je znázorněno použití uzlu pro sériové rozhraní. \cite{zephyr} \cite{devicetree}


\begin{lstlisting}[caption={Příklad devicetree bindings}, label={lst:devicetree_bindings}]
compatible: "manufacturer,serial"

properties:
  reg:
    type: array
    description: UART peripheral MMIO register space
    required: true
  current-speed:
    type: int
    description: current baud rate
    required: true
\end{lstlisting}

\pagebreak

\begin{lstlisting}[caption={Příklad zdrojového souboru devicetree}, label={lst:devicetree_source_example}]
/ {
    soc {
        uart0@40003000 {
            compatible = "manufacturer,serial";
            reg = <0x40003000 0x1000>;
            current-speed=<115200>
        };
    };
};
\end{lstlisting}

\section{Nástroj west}

Operační systém Zephyr je závislý na externích knihovnách. Mezi externí projekty patří například zavaděč MCUboot, kryptografická knihovna Mbed TLS nebo vrstva hardwarové abstrakce pro platformu ESP32, \lstinline{hal_espressif}, kterou spravuje samotná firma Espressif System a jsou pomocí ní implementované ovladače. Nástroj \lstinline{west} umožňuje správu pracovního adresáře, správu externích projektů, jejich aktualizaci a řeší závislostí mezi nimi. Kromě toho poskytuje příkazy pro sestavení projektu, nahrávání firmware do zařízení a ladění.
\cite{zephyr}

\section{Build systém}
Operační systém používá pro sestavení aplikace a jádra nástroj CMake. Sestavení pomocí CMake sestává ze dvou fází, konfigurační fáze a fáze sestavení. V konfigurační fázi jsou zpracovány všechny soubory \lstinline{CMakeLists.txt}. Během toho jsou nastaveny cesty ke zdrojovým \mbox{souborům a} hlavičkovým souborům, kromě toho jsou také zpracovány konfigurační soubory devicetree a Kconfig. Výstupem konfigurační fáze jsou skripty pro nástroje \lstinline{Make} nebo \lstinline{Ninja}, které provedou sestavení projektu. Výsledkem je zkompilovaný projekt ve formátu \acrshort{elf}, se kterým je možné dále pracovat. Ze souboru ve formátu  \acrshort{elf} jsou pomocí nástroje \lstinline{objdump} vytvořeny soubory ve formátu \lstinline{.bin} a nebo \lstinline{.hex}. Tyto soubory je možné ještě dále zpracovávát, pokud jde o aplikaci určenou pro zavaděč MCUboot může být binární soubor následně ještě podepsán. Na platformě ESP32 se firmware, který je zaváděný zavaděčem v paměti \acrshort{rom}, vytváří ze souboru \acrshort{elf} pomocí nástroje \lstinline{elf2image}. 
\cite{esp_idf_guide} \cite{zephyr}

Od verze 3.4 se v \acrshort{rtos} Zephyr nachází rozšíření sysbuild, které dovoluje kombinovat několik build systémů do jednoho. Například lze sestavit aplikaci společně se zavaděčem a oba tyto projekty najednou nahrát do zařízení.
Sysbuild zároveň poskytuje CMake \acrshort{api} pro rozšíření funkcionality. Je možné například sjednotit binární soubory ze všech projektů do jednoho binárního souboru.
\cite{zephyr}

\section{Aktualizace firmware}

Jak již bylo zmíněno na začátku kapitoly, operační systém Zephyr nabízí v základu mnoho funkcionality. Jedním ze stěžejních funkcí, která vývojářům usnadňuje mnoho práce, je podpora pro aktualizace firmware. Způsob aktualizací je dokonce stejný na jakémkoliv podporovaném hardwaru. V praxi to znamená, že například \acrshort{soc} platformy ESP32 se aktualizuje stejným způsobem jako \acrshort{soc} z rodiny STM32 nebo nRF52.
\cite{zephyr}

\subsection{Podpora pro MCUboot}
Zavaděč MCUboot je jedním ze základních kamenů pro podporu aktualizací firmware, zároveň je výchozím zavaděčem pro operační systém Zephyr. Port zavaděče MCUboot je napsaný jako aplikace v operačním systému Zephyr. Díky tomu může využívat již existující ovladače periferií. Zároveň používá další funkcionalitu, kterou Zephyr nabízí, jako je Kconfig, devicetree a stejný build systém. Každá deska specifikuje rozložení paměti pomocí devicetree. Výpis kódu \ref{lst:mcuboot_flash_layout} zobrazuje rozložení flash paměti na desce \lstinline{esp32c3_devkitm}. MCUboot využívá právě definice z devicetree k rozdělení flash paměti na sloty.
\cite{zephyr} \cite{mcuboot}

\begin{lstlisting}[caption={Definice rozložení paměti při použití zavaděče MCUboot \cite{zephyr}}, label={lst:mcuboot_flash_layout}]
&flash0 {
	status = "okay";
	partitions {
		compatible = "fixed-partitions";
		#address-cells = <1>;
		#size-cells = <1>;

		/* Reserve 60kB for the bootloader */
		boot_partition: partition@0 {
			label = "mcuboot";
			reg = <0x00000000 0x0000F000>;
			read-only;
		};

		/* Reserve 1024kB for the application in slot 0 */
		slot0_partition: partition@10000 {
			label = "image-0";
			reg = <0x00010000 0x00100000>;
		};

		/* Reserve 1024kB for the application in slot 1 */
		slot1_partition: partition@110000 {
			label = "image-1";
			reg = <0x00110000 0x00100000>;
		};

		/* Reserve 256kB for the scratch partition */
		scratch_partition: partition@210000 {
			   label = "image-scratch";
			   reg = <0x00210000 0x00040000>;
		};

		storage_partition: partition@250000 {
			label = "storage";
			reg = <0x00250000 0x00006000>;
		};
	};
};
\end{lstlisting}

Konfigurační symbol \lstinline{CONFIG_BOOTLOADER_MCUBOOT} indikuje, že je aplikace určena k zavedení zavaděčem MCUboot. V rámci sestavování aplikace dojde také k vytvoření obrazu ve správném formátu pomocí nástroje \lstinline{imgtool}. Výstupem je binární soubor který je možné nahrát přímo do zařízení a nebo ho použít jako soubor pro aktualizaci firmware. Samotný MCUboot je možné sestavit dvěma způsoby, odděleně jako jakoukoliv jinou aplikaci nebo společně s aplikací pomocí rozšíření sysbuild.
\cite{mcuboot} \cite{zephyr} 

\subsection{Nástroj MCUmgr}

MCUmgr je nástroj pro vzdálenou správu vestavných zařízení. Nástroj je modulárně koncipovaný, jednotlivé moduly poskytují různou funkcionalitu. V základu nástroj obsahuje moduly pro správu souborového systému, přístup k terminálu zařízení, aktualizace firmwaru a další. Aktualizace firmwaru jsou kompatibilní se zavaděčem MCUboot. Se zařízeními je možné komunikovat pomocí sériového rozhraní, bluetooth nebo po sítí pomocí UDP.
\cite{mcumgr}

MCUmgr sestává ze dvou částí, klienta a SMP serveru. Klientem je typicky počítač nebo telefon, pomocí kterého se zařízení spravuje. Na straně zařízení pak musí být implementován SMP server. MCUmgr lze také používat pro komunikaci mezi dvěma mikroprocesory, v takovém případě jedno zařízení funguje jako klient a na druhém je aktivní server. Operační systém Zephyr obsahuje implementaci klienta i SMP serveru.
\cite{zephyr} \cite{mcumgr}



Pro PC existuje klient MCU Manager \acrshort{cli}, prostřednictvím kterého lze komunikovat se zařízeními obsahující SMP server. Na příkladu \ref{lst:mcumgr_fw_update} je ukázáno vytvoření připojení prostřednictvím sériového rozhraní a nahrání aktualizace do zařízení. Podpora pro MCUmgr v operačním systému Zephyr využívá definice rozdělení flash paměti z devicetree pro zjištění do jakého slotu má být aktualizace nahrána. 

Od firmy Nordic Semiconductor je k dispozici mobilní aplikace nRF Connect Device Manager, která implementuje klienta pro mobilní zařízení. Komunikace se zařízením je realizována pomocí technologie bluetooth.
\cite{mcumgr} \cite{nrf_device_manager}


\begin{lstlisting}[caption={Aktualizace firmware pomocí MCUmgr}, label={lst:mcumgr_fw_update}]
# Create connection
$ mcumgr conn add acm0 type="serial"  \
    connstring="dev=/dev/ttyACM0,baud=115200,mtu=512"

# upload image to secondary slot
$ mcumgr -c acm0 image upload <binary file>

# confirm update
$ mcumgr -c acm0 image confirm <hash>

# restart device
$ mcumgr -c acm0 reset
\end{lstlisting}