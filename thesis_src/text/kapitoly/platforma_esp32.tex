\chapter{Platforma ESP32}
\label{chapter:esp32_platform}
ESP32 je série levných mikroprocesorů s bohatou síťovou konektivitou od čínského výrobce Espressif Systems. 
Čip ESP32 byl představen jako nástupce čipu ESP8266. Starší ESP8266 bylo určeno především jako externí síťový koprocesor s \acrshort{wifi} konektivitou. 
Pro aplikaci bylo nutné mít další mikroprocesor. 
V roce 2014 firma Espressif vydala \acrshort{sdk}, které dovolilo naprogramovat ESP8266 přímo a odstranila se tak potřeba mít separátní procesor pro aplikaci.
Stále zde však byla limitace v počtu \acrshort{gpio} pinů a nedostatku periferií.
\cite{espressif}

Tyto nedostatky se firma snaží řešit a v roce 2016 představuje \acrshort{soc} ESP32, které přináší rychlejší dvou-jádrový procesor, více paměti \acrshort{ram}, téměř dvojnásobný počet \acrshort{gpio} pinů a modernější bezdrátovou konektivitu. 
Nově je zde podpora pro Bluetooth LE ve verzi 4.2.
\cite{espressif}

V dnešní době zahrnuje rodina několik variant ESP32. Mikroprocesory využívají jádra s architekturou Xtensa a nebo RISC-V. 
Síťová konektivita zahrnuje především technologii \acrshort{wifi} a Bluetooth, včetně Bluetooth LE.
Nově jsou v portfóliu také \acrshort{soc} s konektivitou pomocí IEEE 802.15.4, jde o čipy ESP32-H2 a ESP32-C6.
Vzhledem ke své konektivitě se tyto procesory často používají v \acrshort{iot} aplikacích.
\cite{espressif}


Oficiální vývojová platforma je ESP-IDF, která je založená na operačním systému reálného času FreeRTOS. Není však třeba využívat pouze softwarovou platformu ESP-IDF, kromě ni existuje podpora také v operačním systému Zephyr a Apache NuttX.
\cite{espressif}

\section{SoC ESP32-C3}
V rámci této práce autor používá především čip ESP32-C3. Výsledné řešení bylo v průběhu portované také na čip ESP32-S3. V počátku práce byl však čip ESP32-C3 ten, pro který bylo řešení navrhováno. většina věcí je však na všech čipech ESP32 řešena stejně, proto je zde uveden jen popis čipu ESP32-C3.\cite{esp32c3_reference} \cite{esp32s3_reference} \cite{esp32c3_datasheet} \cite{esp32s3_datasheet}

ESP32-C3 je čip s 32-bitovým jedno-jádrovým procesorem s architekturou RISC-V, který může být taktován maximálně na 160 MHz. Procesor je doplněn pamětí \acrshort{ram} o velikosti \mbox{400 KB}. Z celkové velikosti paměti je 16 KB možné použít jako cache paměť pro přístup k instrukcím. Dále je zde 8 KB \acrshort{rtc} \acrshort{ram} paměti. Tato paměť si udrží svá data i v případě, že je čip uveden do deep-sleep režimu. \cite{esp32c3_datasheet}

Na čipu se také nachází 384 KB paměti \acrshort{rom}, do které je z výroby \uv{vypálen} zavaděč a zdrojový kód, který je přístupný z aplikace. Zavaděč v \acrshort{rom} paměti může být označován také jako nultý zavaděč. 
Tuto paměť není možné žádným způsobem uživatelsky modifikovat. 
K čipu je možné připojit externí flash paměť pomocí rozhraní \acrshort{spi}. Flash paměť je možné pomocí \acrshort{mmu} namapovat přímo do adresního prostoru. 
Díky tomu lze instrukce a data dostupná pouze pro čtení linkovat přímo do externí flash paměti.
\cite{esp32c3_reference} \cite{esp_idf_guide}

Bezdrátovou konektivitu zajišťuje technologie \acrshort{wifi} a Bluetooth. ESP32-C3 podporuje \break \acrshort{wifi} standardu IEEE 802.11b/g/n a Bluetooth LE ve verzi 5 včetně rozšíření Extended Advertising a Long Range. Čip obsahuje periferie rozhraní \acrshort{spi}, \acrshort{uart}, \acrshort{i2c}, \acrshort{i2s} a mnoho dalších.
\cite{esp32c3_datasheet}

\begin{figure}[h!]
    \centering
    \includegraphics[width=.87\textwidth]{text/grafika/esp32c3_block.png}
    \caption{Blokový diagram SoC ESP32-C3 \cite{esp32c3_datasheet}}
    \label{fig:esp32c3_block}
\end{figure}


\section{Paměť}

Mapa adresního prostoru čipu ESP32-C3 je zobrazena na obrázku \ref{fig:esp32c3_address_map}. Na čipech ESP32 architektury Xtensa je adresní prostor řešený úplně stejně, liší se jen v hodnotách adres. Přístup do pamětí (externí flash, \acrshort{rom}, \acrshort{ram}) je realizován pomocí dvou sběrnic, instrukční a datové. 
Pomocí instrukční sběrnice lze přistupovat také k datům, ale adresa musí být zarovnaná na 4 byty, oproti tomu datová sběrnice umožňuje přistupovat k datům po 1 bytu.
\cite{esp32c3_reference}
\cite{esp32s3_reference}

Na čipu jsou integrovány následující typy pamětí: 
\begin{description} [style=nextline]
    \item[ROM] 
    Jde paměť dostupnou pouze pro čtení, do které jsou z výroby zapsáná data. 
    Nachází se zde ROM zavaděč společně s daty a instrukcemi systémového software (část kódu pro přístup k rádiové periferii, funkce pro čtení flash paměti, část standardní knihovny jazyka C, ...).
    Na čipu se nachází 384 KB paměti tohoto typu. Prvních 256 KB je dostupných pouze skrz instrukční sběrnici a zbylých 128 KB pomocí instrukční a datové sběrnice.
    
    \item[RAM] 
    Ne všechny části paměti RAM jsou dostupné pomocí instrukční a datové sběrnice zároveň. 
    Prvních 16 KB paměti, která jde zároveň použita jako instrukční cache, je dostupná pouze pomocí instrukční sběrnice. 
    Ke zbylým 384 KB je možné přistupovat pomocí obou sběrnic.

    \item[RTC RAM]
    Jde o standardní RAM paměť, která je persistentní i v deep-sleep režimu. 
    Na čipu je k dispozici 8 KB této paměti. Přistupovat k ní lze pomocí jedné sběrnice společné pro data i instrukce. \cite{esp32c3_reference}
\end{description}

K čipu je také možné pomocí \acrshort{spi} připojit externí flash paměť. Paměť lze díky \acrshort{mmu} namapovat přímo do adresního prostoru. Až 8 MB lze namapovat jako data, dalších 8 MB jako instrukce.
Flash pamět se mapuje po blocích velkých 64 KB. Aplikace se standardně nenachází pouze v paměti \acrshort{ram}, ale většina instrukcí a data pro čtení jsou uložena v externí flash paměti. Během linkování aplikace je symbolům nacházejícím se ve flash paměti přiražena odpovídající virtuální adresa. Zavaděč během zavádění aplikace namapuje části flash paměti do adresního prostoru. Z důvodu mapování je nutné, aby byl obraz aplikace uložen v paměti na adrese, která je násobkem 64 KB (velikost stránky \acrshort{mmu}).
\cite{esp32c3_reference}


Pokud se v této práci vyskytne výraz \acrshort{iram}, \acrshort{dram}, je tím myšlena paměť \acrshort{ram}, ke které se přistupuje pomocí instrukční, respektive datové sběrnice. Obdobně budu označovat také externí flash paměť pomocí zkratky \acrshort{irom} / \acrshort{drom}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=.9\textwidth]{text/grafika/esp32c3_address_map.png}
    \caption{Mapa adresního prostoru  \acrshort{soc} ESP32-C3 \cite{esp32c3_reference}}
    \label{fig:esp32c3_address_map}
\end{figure}

%\subsection{Obsah paměti ROM}
%??? ???

\section{eFuse}

Všechny \acrshort{soc} platformy ESP32 obsahují speciální jednorázově programovatelnou paměť eFuse (elektronické pojistky). 
Paměť je bitově orientovaná. Při práci s eFuse je třeba velké opatrnosti. 
Zápis je jednorázová operace.
Jakmile je bit nastaven na logickou jedničku, neexistuje žádný způsob jak bit resetovat zpět do nuly. 

\pagebreak

Dle technické dokumentace obsahuje \acrshort{soc} ESP32-C3 4096 jednorázově programovatelných bitů, avšak dále se v dokumentu uvádí, že eFuse jsou rozděleny do 11 bloků po 256 bitech. To odpovídá pouze 2816 bitům. \cite{esp32c3_reference}
\begin{description} [style=nextline]
    \item[Blok 0-2] 
    Bloky jsou vyhrazené pro systémové účely. Nastavením některých bitů lze například aktivovat secure boot, vypnout rozhraní \acrshort{jtag}, zakázat nahrávání firmwaru pomocí \acrshort{uart} nebo nastavit ochranu před čtením/zápisem určitým eFuse blokům, Také se zde nachází výrobní data specifická pro konkrétní čip, zejména \acrshort{mac} adresa a kalibrace \acrshort{adc}.
    
    \item[Blok 3]
    Je vyhrazen pro uživatelská data.

    \item[Blok 4-9]
    Bloky jsou vyhrazené pro ukládání klíčů. Mohou zde být uloženy klíče pro secure boot a pro šifrování externí flash paměti. Výjimku tvoří blok 9, do kterého není možné uložit klíč pro šifrování paměti, důvodem je hardwarová chyba. Pokud některý z bloků není obsazen klíčem, je možné jej využít pro uživatelská data.
    
    \item[Blok 10]
    Dle  dokumentace je opět vyhrazený pro systémové využití. V tomto bloku se ovšem nenachází žádná data a nikde není zmíněno jeho využití, nejspíš ho lze také využít pro uživatelská data. \cite{esp_idf_guide}
\end{description}


\section{Formát obrazu firmware}
\label{section:esp32_image_format}
Aby byl \acrshort{rom} zavaděč schopný zavést firmware, je třeba dodržet specifický formát. Tento formát je společný pro všechny čipy platformy ESP32. Struktura firmware je vyobrazena na obrázku \ref{fig:esp32_image_format}.

\begin{figure}[h!]
    \centering
    \includesvg[width=\textwidth]{text/grafika/esp32_image_format.svg}
    \caption{Formát obrazu firmware pro ESP32 \cite{esp_idf_guide}}
    \label{fig:esp32_image_format}
\end{figure}

Hlavička firmware je rozdělena na dvě hlavičky, základní a rozšířenou. V hlavičce jsou obsaženy následující údaje:
\begin{description}[style=nextline]
    \item[Magický byte] 
    Podle něj je možné detekovat, zda se v paměti nachází firmware v daném formátu.
    
    \item[Počet segmentů]
    Udává kolik je v obraze přítomných segmentů s daty.

    \item[Režim SPI flash paměti]
    Udává v jakém SPI režimu je externí paměť zapojena (\acrshort{qio}, \acrshort{qout}, \acrshort{dio}, \acrshort{dout}).
    
    \item[Velikost flash paměti a její frekvence]
    Jeden byte. Horní 4 bity udávají velikost paměti, dolní 4 bity její frekvenci.

    \item[Adresa reset vektoru]
    Adresa v paměti, na kterou se skočí po úspěšném zavedení firmware.

    \item[Dodatečné informace k zapojení flash]
    Určuje, jak je zapojena externí flash pomocí \acrshort{spi} k čipu, pokud je tomu jinak oproti výchozímu stavu.

    \item[ID čipu] 
    Určuje, pro jaký čip je firmware určený.
    
    \item[Minimální revize čipu] 
    Minimální revize, se kterou je firmware kompatibilní.

    \item[Indikace přítomnosti hashe] 
    Indikuje zda se v patičce nachází hash obrazu firmwaru \cite{esp_idf_guide}
\end{description}

Po hlavičce následují samotné datové segmenty. Podle cílové adresy segmentu se zavaděč rozhodne zda daný segment nahraje do paměti \acrshort{ram}, či namapuje do adresního prostoru. Pokud cílová adresa odpovídá paměti \acrshort{iram} či \acrshort{dram} je segment nahrán. V případě, že adresa odpovídá \acrshort{irom} nebo \acrshort{drom}, segment je namapován do adresního prostoru.
V patičce obrazu se vždy nachází kontrolní součet a volitelně také, hash obrazu.
\cite{esp_idf_guide}

Již byla zmíněna přítomnost dvou sběrnic pro přístup do paměti. S tím také souvisí formát firmware. 
Část firmware je do paměti nahrávána pomocí instrukční sběrnice a část pomocí datové. RAM paměť je tedy vždy rozdělena na minimálně dva segmenty. 
Stejně je tomu v případě mapování, zvlášť jsou namapovány segmenty s instrukcemi a segmenty obsahující data, opět minimálně dva segmenty. 
Segmenty, které se do adresního prostoru mapují musí navíc splňovat podmínku zarovnání na velikost stránky, o to se stará nástroj \lstinline{elf2image}, pomocí něj se ze souboru ve formátu\acrshort{elf} vytvoří binární soubor v právě zmiňovaném formátu. 
\cite{esp32c3_reference} \cite{esp_idf_guide}


\section{Secure boot}

Secure boot je bezpečnostní funkce, která pomáhá chránit zařízení před spuštěním neoprávněného kódu. 
Pokud je secure boot aktivován, zařízení je schopné spustit pouze podepsaný firmware z důvěryhodného zdroje. 
Původní čip ESP32 měl implementovaný secure boot na bázi symetrické šifry \acrshort{aes}, jeho použití již není ze strany výrobce doporučené.
Modernější čipy a novější revize ESP32 používají secure boot v2 na bázi asymetrické šifry \acrshort{rsa}. Soukromý klíč slouží k podepsání firmwaru, veřejným klíčem je možné podpis ověřit.
\cite{esp_idf_guide}

Aby mělo smysl využívat secure boot, je nutné ověřovat každou část software, kterou může procesor spustit. 
V případě použití ESP-IDF \acrshort{sdk} jde o jakýkoliv aplikační firmware a zavaděč. 
Zavaděč v paměti \acrshort{rom} není nijak podepisován a není to potřeba, protože se nachází v interní \acrshort{rom} paměti a není možné ho žádným způsobem přepsat.
\cite{embedded_security} \cite{esp_idf_guide}

\subsection{Formát podepsaného firmware}

Podepsaný firmware se od nepodepsaného téměř neliší, jen je na jeho konec přidán blok obsahující podpis.
Struktura podpisu je popsána v tabulce \ref{tab:esp32_signature_block}. Hodnoty \lstinline{R} a \lstinline{N} jsou odvozeny z modulu, respektive exponentu a jsou používány pro hardwarově akcelerované násobení v montgomerygo doméně.
\cite{esp_idf_guide}

\begin{table}[h!]
\centering
\caption{\label{tab:esp32_signature_block} Struktura podpisového bloku \cite{esp_idf_guide}}
    \begin{tabular}{ | l | l | l |}
      \hline
      \textbf{offset} & \textbf{velikost (bytů)} & \textbf{popis} \\
      \hline
      \hline
      0 & 1 & Magický byte \\
      \hline
      1 & 1 & Verze secure boot \\
      \hline
      2 & 2 & Výplň \\
      \hline
      4 & 32 & SHA-256 hash obrazu firmware (bez tohoto bloku) \\
      \hline
      36 & 384 & Modul používaný pro ověření podpisu \\
      \hline
      420 & 4 & Exponent používaný pro ověření podpisu \\
      \hline
      424 & 384 & Přepočítaná hodnota R pro násobení v montgomeryho doméně  \\
      \hline
      808 & 4 & Přepočítaná hodnota M pro násobení v montgomeryho doméně \\
      \hline
      812 & 384 & Výsledek podpisu firmwaru pomocí RSA \\
      \hline
      1196 & 4 & CRC32 předchozích 1196 bytů \\
      \hline
      1200 & 16 & Doplnění do 1216 bytů \\
      \hline
    \end{tabular}
\end{table}

Součástí podpisového bloku je také veřejný klíč, který slouží k verifikaci firmware. To znamená, že je možné ověřit firmware podepsaný jakýmkoliv privátním klíčem. 
Veřejný klíč se vždy nejprve ověřuje.
Hash veřejného klíče je uložen v eFuse. Do eFuse je možné uložit až tři klíče. 
Před ověřením firmware se vždy zkontroluje zda se veřejný klíč v podpisovém bloku shoduje s důvěryhodným klíčem uloženým v eFuse. 
V případě, že se klíče neshodují není firmware důvěryhodný a zavádění končí chybou. 
Firmware je tedy důvěryhodný pouze pokud je podepsán důvěryhodným klíčem a zároveň je úspěšně verifikován jeho podpis.
\cite{esp_idf_guide}

\subsection{Secure boot a MMU}
Při použití secure boot v2 se doporučuje obraz firmware doplnit nulami tak, aby jeho velikost byla dělitelná velikostí stránky \acrshort{mmu}. 
Standardně je stránka velká 64 KB. 
Podpis díky tomu bude zarovnaný na velikost stránky. 
Důvodem je, že zavaděč během zpracovávání podpisu mapuje flash paměť do adresního prostoru. 
Při nezarovnání podpisu by se v adresním prostoru mohl objevit i nedůvěryhodný kód, což představuje bezpečnostní riziko. 
Doplnění je do obrazu firmware možné přidat během vytváření obrazu pomocí nástroje \lstinline{elf2image}, stačí použít parametr \lstinline{--secure-pad-v2}.
\cite{esp_idf_guide}

\subsection{Postup při ověření firmware}
Postup během ověřování firmware je následující:

\begin{enumerate}
  \item Po startu čipu je spuštěn zavaděč nacházející se v paměti ROM.
  \item Pokud není secure boot aktivován, verifikace je přeskočena. V opačném případě se pokračuje následujícími kroky.
  \item Zavaděč ověří platnost podpisového bloku, to zahrnuje ověření magické bytu a ověření kontrolního součtu.
  \item Následně je spočítán hash firmware a porovnán s hash uloženým v podpisovém bloku.
  \item Jako další krok dojde k ověření veřejného klíče, spočítá se hash veřejného klíče, který se nachází v podpisovém bloku, a následně se porovná s klíči uloženými v eFuse.
  \item Ověří se podpis firmware.
  \item Pokud předchozí kroky proběhly úspěšně je spuštěn požadovaný firmware. \cite{esp_idf_guide}
\end{enumerate}

\subsection{Aktivace secure boot}
Pro aktivaci secure boot je nutné do paměti eFuse vypálit bit \lstinline{SECURE_BOOT_EN} a hash veřejného klíče.
Součástí vývojářského balíku jsou nástroje, které usnadňují generování klíčů, jejich zápis do efuse a podepisování firmwaru.
\cite{esp_idf_guide}

\begin{lstlisting}[caption={Vygenerování klíče pomocí espsecure.py}]
$ espsecure.py generate_signing_key key.pem --scheme rsa3072
\end{lstlisting}

\begin{lstlisting}[caption={Vygenerování klíče pomocí openssl}]
$ openssl genrsa -out key.pem 3072
\end{lstlisting}

\begin{lstlisting}[caption={Zápis klíčů do efuse}]
$ espefuse.py burn_key_digest \
            BLOCK_KEY0 key_0.pem  SECURE_BOOT_DIGEST0  \
            BLOCK_KEY1 key_1.pem  SECURE_BOOT_DIGEST1  \
            BLOCK_KEY2 key_2.pem  SECURE_BOOT_DIGEST2
\end{lstlisting}

Příkaz \lstinline{burn_key_digest} zároveň automaticky nastaví odpovídajícím blokům ochranu proti zápisu.
Jako parametr příkazu je možné použít veřejný nebo privátní. Z privátní klíče se vygeneruje veřejný, který je následně zapsán do eFuse. \cite{esp_idf_guide}

\begin{lstlisting}[caption={Aktivace secure boot}]
$ espefuse.py burn_efuse SECURE_BOOT_EN 1
\end{lstlisting}

Po aktivaci secure boot je možné spustit pouze firmware podepsaný odpovídajícím privátním klíčem. Příkaz \lstinline{espsecure.py sign_data} analyzuje firmware a na jeho konec zapíše blok obsahující podpis. \cite{esp_idf_guide}

\begin{lstlisting}[caption={Podepsání firmware}]
$ espsecure.py sign_data \
            --version 2 \
            --pub-key key.pub \
            --output firmware.signed.bin \
            firmware.bin
\end{lstlisting}


\subsection{Šifrování flash paměti}

Secure boot lze používat i bez šifrování flash paměti. Pokud však útočník získá fyzický přístup k zařízeni, může po ověření podpisu firmwaru a před jeho spuštěním změnit obsah flash paměti a zavaděč tak spustí nedůvěryhodný firmware. Tento problém řeší šifrování flash paměti, veškerá přenášená data po SPI mezi procesorem a paměťovým čipem jsou šifrovány pomocí AES. Klíč je opět uložen veFuse a tentokrát je chráněn proti čtení i zápisu, stále ale zůstává přístupný hardwarové jednotce, která se stará o šifrování dat.
Společně se secure boot je silně doporučeno zapnutí šifrování externí flash paměti.
\cite{embedded_security} \cite{esp_idf_guide}

\section{Podpůrné nástroje}
Pro práci s čipy platformy ESP32 je distribuován balíček \lstinline{esptool.py}, který obsahuje nástroje pro vytváření obrazu firmwaru, podepisování firmwaru a komunikaci s \acrshort{rom} zavaděčem. Právě prostřednictvím \acrshort{rom} zavaděče je možné číst a zapisovat data do externí flash paměti. Také je pomocí něj možné číst a zapisovat do eFuse. Nástroje jsou schopné komunikovat s čipem pouze pokud je uveden do download režimu. Na vývojové desce se čip do download režimu restartuje automaticky při použití nástroje \lstinline{esptool.py}.
\cite{esp_idf_guide}

Balíček nástrojů \lstinline{esptool.py} je distribuován jako Python balíček. Instalaci je možné provést pomocí správce balíčků \lstinline{pip}.

\begin{lstlisting}[caption={Instalace esptool.py}]
$ pip install esptool.py
\end{lstlisting}


\subsection{Vytvoření obrazu firmware}

Příkaz \lstinline{elf2image} slouží k vytvoření obrazu firmwaru.
Nástroj zkonvertuje sestavený firmware ve formátu \acrshort{elf} do binárního souboru ve formátu kompatibilním s \acrshort{rom} zavaděčem.
\cite{esp_idf_guide}

\begin{lstlisting}[caption={Vytvoření obrazu firmwaru}]
$ esptool.py --chip esp32c3 elf2image my_app.elf
\end{lstlisting}

\subsection{Nahrávání firmware}

Pomocí \lstinline{esptool.py} lze rovněž číst a zapisovat do externí flash paměti. Použití je především pro zápis firmwaru do flash paměti.
\cite{esp_idf_guide}

\begin{lstlisting}[caption={Nahrání firmware pomocí esptool.py přes rozhraní UART}]
$ esptool.py --chip auto write_flash 0x10000 filename.bin
\end{lstlisting}

Kromě nahrávaní firmwaru prostřednictvím nástroje \lstinline{esptool.py} a tedy skrze sériový port je možné firmware nahrát také pomocí rozhraní JTAG. K tomu je nutné mít stažené OpenOCD.
\cite{esp_idf_guide}

\begin{lstlisting}[caption={Nahrání firmware pomocí OpenOCD přes rozhraní JTAG}]
$ openocd -f board/esp32c3-builtin.cfg \
          -c "program_esp filename.bin 0x10000 verify exit"
\end{lstlisting}

\subsection{Čtení a zápis eFuse}

Součástí balíku \lstinline{esptool.py} je také nástroj \lstinline{espefuse.py}, který umožňuje čtení a zápis dat do paměti eFuse. Následující příkaz vypálí bity 15, 16, 17, 18, 19, 20 v bloku 2 na logickou 1.
\begin{lstlisting}[caption={Vypálení bitů paměti eFuse}]
$ espefuse.py burn_bit BLOCK2 15 16 17 18 19 20
\end{lstlisting}
Data je množné vypálit také na základě obsahu binárního souboru.
\begin{lstlisting}[caption={Vypálení eFuse na základě obsahu binárního souboru}]
$ espefuse.py burn_block_data BLOCK3 efuse_data.bin
\end{lstlisting}
Pro zobrazení aktuálního stavu eFuse paměti slouží příkaz \lstinline{espefuse.py summary}.

\begin{lstlisting}[caption={Zobrazení obsahu eFuse}]
$ espefuse.py summary
\end{lstlisting}