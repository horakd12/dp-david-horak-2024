# Updatable MCUboot for ESP32

This document describes how to setup environment, build and flash sample and run tests.

## Prepare environment

Use the package manager pip to intall neccessary tools.

```bash
pip install west
pip install -r zephyr/scripts/requirements.txt
```

Initialize west workspace and update dependencies:
```bash
west init -l zephyr
west update
```

Add modified modules to `ZEPHYR_EXTRA_MODULES` variable:
```bash
export ZEPHYR_EXTRA_MODULES="$(pwd)/immutable_bootloader_support;$(pwd)/bootloader/mcuboot;$(pwd)/modules/hal/espressif"
```

## Building sample
Building and flashing sample is done using west. Sample is located in `immutable_bootloader_support/samples/app_with_updatable_mcuboot`.
Currenly supported boards are `esp32c3_devkitm` and `esp32s3_devkitm`

```bash
west build -b <board> --sysbuild immutable_bootloader_support/samples/app_with_updatable_mcuboot
```

After building sample project it can be uploaded to development kit:

```bash
west flash
```
## Running tests

Test is locatd in `immutable_bootloader_support/tests/mcumgr_update`

Currenly supported boards are `esp32c3_devkitm` and `esp32s3_devkitm`

Test can be executed using twister:

```bash
west twister --device-testing --device-serial /dev/ttyUSB0 --device-serial-baud 115200 -p <board> -T immutable_bootloader_support/tests/mcumgr_update/ -W -vv --west-flash
```