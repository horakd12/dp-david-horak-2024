import subprocess
from pathlib import Path

class DTWestFlasher:
    def __init__(self, build_dir):
        self.build_dir = build_dir

    def flash(self, domain=None, hex_file=None, erase=None):
        command = [
            'west', 'flash', '--skip-rebuild',
            '--build-dir', str(self.build_dir)
        ]

        if erase != None:
            command.append('--erase')

        if domain != None:
            command.append('--domain')
            command.append(str(domain))

        if hex_file != None:
            command.append('--hex-file')
            command.append(str(hex_file))

        subprocess.run(command, check=True)
