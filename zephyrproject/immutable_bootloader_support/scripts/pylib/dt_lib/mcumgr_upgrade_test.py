from dataclasses import dataclass
from twister_harness import MCUmgr
import time

from dt_lib.dt_flasher import DTWestFlasher
from dt_lib.dt_fw_builder import DTFirmwareBuilder
from dt_lib.dt_fwinfo_shell import DTFWInfoShell, DTFWInfo


@dataclass
class MCUbootUpdateTestParam:
    initial_slot: int = 0
    new_mcuboot_version: int = 255
    invalid_slot: bool = False
    invalid_signature: bool = False
    update_successful: bool = False
    reuse_build_folder: bool = False


mcuboot_update_test_default_config = [
    # Correct update
    MCUbootUpdateTestParam(0, 255, False, False, True, False),
    MCUbootUpdateTestParam(1, 255, False, False, True, True),

    # Update to invalid slot
    MCUbootUpdateTestParam(0, 255, True, False, False, True),
    MCUbootUpdateTestParam(1, 255, True, False, False, True),

    # Update to lower version
    MCUbootUpdateTestParam(0, 1, False, False, False, False),
    MCUbootUpdateTestParam(1, 1, False, False, False, True),

    # Invalid signature key for immutable bootloader
    MCUbootUpdateTestParam(0, 255, False, True, False, False),
    MCUbootUpdateTestParam(1, 255, False, True, False, True),
]


class McumgrUpdateTest:
    def __init__(self, mcumgr: MCUmgr, fw_builder: DTFirmwareBuilder,
                 fwinfo_shell: DTFWInfoShell, build_dir: str):
        self.mcumgr = mcumgr
        self.fw_builder = fw_builder
        self.fwinfo_shell = fwinfo_shell
        self.build_dir = build_dir

    def _prepare_device(self):
        flasher = DTWestFlasher(self.build_dir)
        flasher.flash()

    def _do_update(self, file, reset_after_update):
        self.mcumgr.image_upload(file)
        new_image_hash = self.mcumgr.get_hash_to_test()
        self.mcumgr.image_confirm(new_image_hash)
        self.mcumgr.reset_device()

        time.sleep(12)

        if reset_after_update:
            self.mcumgr.reset_device()
            time.sleep(3)

        return new_image_hash

    def run_test(self):
        pass


class MCUbootUpdateTest(McumgrUpdateTest):
    def __init__(self, test_config: MCUbootUpdateTestParam,
                 mcumgr: MCUmgr, fw_builder: DTFirmwareBuilder,
                 fwinfo_shell: DTFWInfoShell, build_dir: str):
        self.test_config = test_config
        self.invalid_immutable_key = ""
        super().__init__(mcumgr, fw_builder, fwinfo_shell, build_dir)

    def _prepare_device(self, mcuboot_slot):
        pass

    def set_invalid_immutable_key(self, key):
        self.invalid_immutable_key = key

    def run_test(self):
        self._prepare_device(self.test_config.initial_slot)

        time.sleep(3)

        fwinfo = self.fwinfo_shell.get()

        if self.test_config.invalid_signature:
            self.fw_builder.set_immutable_sign_key(self.invalid_immutable_key)

        mcuboot_slot = 1 - self.test_config.initial_slot

        if self.test_config.invalid_slot:
            mcuboot_slot = self.test_config.initial_slot

        self.fw_builder.set_mcuboot_version(self.test_config.new_mcuboot_version)

        if self.test_config.reuse_build_folder:
            self.fw_builder.set_build_dir(f"{self.build_dir}/build_test")
        else:
            self.fw_builder.build(f"{self.build_dir}/build_test")

        new_mcuboot = self.fw_builder.get_mcuboot_path(mcuboot_slot)

        self._do_update(new_mcuboot, True)

        if self.test_config.update_successful:
            fwinfo.mcuboot_version = self.test_config.new_mcuboot_version
            fwinfo.mcuboot_slot = 1 - self.test_config.initial_slot

        self.fwinfo_shell.verify(fwinfo)


class ESP32MCUbootUpdateTest(MCUbootUpdateTest):
    def _prepare_device(self, mcuboot_slot):
        fw_flasher = DTWestFlasher(self.build_dir)
        fw_flasher.flash(domain="mcumgr_update", erase=True)

        if mcuboot_slot == 0:
            fw_flasher.flash(domain="mcuboot")
        else:
            fw_flasher.flash(domain="mcuboot_s1")

        fw_flasher.flash(domain="immutable_esp32")


class NrfMCUbootUpdateTest(MCUbootUpdateTest):
    def _prepare_device(self, mcuboot_slot):
        fw_flasher = DTWestFlasher(self.build_dir)
        fw_flasher.flash(hex_file=f"{self.build_dir}/zephyr/app_signed.hex", erase=True)

        if mcuboot_slot == 0:
            fw_flasher.flash(hex_file=f"{self.build_dir}/zephyr/signed_by_mcuboot_and_b0_s0_image_signed.hex")
        else:
            fw_flasher.flash(hex_file=f"{self.build_dir}/zephyr/signed_by_mcuboot_and_b0_s1_image_signed.hex")

        fw_flasher.flash(hex_file=f"{self.build_dir}/zephyr/b0_container.hex")


@dataclass
class AppUpdateTestParam:
    invalid_signature: bool = False


app_update_test_default_config = [
    # Correct update
    AppUpdateTestParam(False),

    # Invalid signature
    AppUpdateTestParam(True),
]


class AppUpdateTest(McumgrUpdateTest):
    def __init__(self, test_config: AppUpdateTestParam,
                 mcumgr: MCUmgr, fw_builder: DTFirmwareBuilder,
                 fwinfo_shell: DTFWInfoShell, build_dir: str):
        self.test_config = test_config
        self.invalid_mcuboot_key = ""
        super().__init__(mcumgr, fw_builder, fwinfo_shell, build_dir)

    def set_invalid_signature_key(self, key):
        self.invalid_mcuboot_key = key

    def run_test(self):
        self._prepare_device()

        time.sleep(3)

        fwinfo = self.fwinfo_shell.get()

        if self.test_config.invalid_signature:
            self.fw_builder.set_mcuboot_sign_key(self.invalid_mcuboot_key)

        self.fw_builder.build(f"{self.build_dir}/build_test")

        new_app = self.fw_builder.get_app_path()

        new_hash = self._do_update(new_app, False)

        if not self.test_config.invalid_signature:
            fwinfo.firmware_hash = new_hash

        self.fwinfo_shell.verify(fwinfo)
