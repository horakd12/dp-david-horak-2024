import subprocess
from pathlib import Path

class DTFirmwareBuilder:
    def __init__(self, app_dir, board):
        self.app_dir = app_dir
        self.board = board

        self.build_dir = None
        self.west_args = []
        self.cmake_args = []


    def add_west_arg(self, arg):
        self.west_args.append(arg)

    def add_cmake_arg(self, arg):
        self.cmake_args.append(arg)

    def set_build_dir(self, build_dir):
        self.build_dir = build_dir

    def build(self, build_dir):
        self.build_dir = build_dir

        command = [
            'west', 'build',
            '-b', self.board,
            str(self.app_dir),
            '--build-dir', str(build_dir),
            '-p'
        ]

        command.extend(self.west_args)
        command.append('--')
        command.extend(self.cmake_args)

        subprocess.run(command, check=True)

    def set_mcuboot_version(self, key_file):
        pass

    def set_mcuboot_sign_key(self, key_file):
        pass

    def set_immutable_sign_key(self, key_file):
        pass

    def get_app_path(self):
        pass

    def get_mcuboot_path(self, slot):
        pass

    
class DTESP32FirmwareBuilder(DTFirmwareBuilder):
    def __init__(self, app_dir, board):
        super().__init__(app_dir, board)
    
    def set_mcuboot_version(self, version):
        super().add_cmake_arg(f"-Dmcuboot_CONFIG_DT_ESP32_IMG_INFO_VERSION={version}")

    def set_mcuboot_sign_key(self, key_file):
        super().add_cmake_arg(f'-DSB_CONFIG_BOOT_SIGNATURE_KEY_FILE="{key_file}"')

    def set_immutable_sign_key(self, key_file):
        super().add_cmake_arg(f'-Dmcuboot_CONFIG_ESPSECURE_SIGNATURE_KEY="{key_file}"')

    def get_app_path(self):
        return f"{self.build_dir}/{Path(self.app_dir).name}/zephyr/zephyr.signed.bin"

    def get_mcuboot_path(self, slot):
        if slot == 0:
            return f"{self.build_dir}/mcuboot/zephyr/zephyr.signed.bin"
        else:
            return f"{self.build_dir}/mcuboot_s1/zephyr/zephyr.signed.bin"
