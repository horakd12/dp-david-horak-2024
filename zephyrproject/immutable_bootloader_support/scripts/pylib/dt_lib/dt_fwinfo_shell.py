import binascii
import io
import logging
from typing import Any, Callable, Optional, List, Union
import tqdm
from concurrent.futures import Future, ThreadPoolExecutor
import re
from serial import Serial
import os


## Part of this script is not my own implementation
## The folowing code is part of company intarnal codebase
## It starts here

def connect_serial(device: str,
                   wait_timeout_s=60,
                   **kwargs) -> Optional[Serial]:
    """Tries to create a pyserial insance, waiting for the device to appear."""
    sleep_t = 0.01
    attempts_n = int(wait_timeout_s / sleep_t)
    for _ in tqdm.trange(attempts_n,
                         desc="Connecting to " + device,
                         unit="attempts",
                         delay=0.1):
        if not os.path.exists(device):
            time.sleep(0.01)
            continue
        try:
            return Serial(device, **kwargs)
        except:
            continue
    return None

from typing import Callable, Any, Type, TypeVar
from concurrent.futures import Future

_T = TypeVar("_T")
_K = TypeVar("_K")

def map_future(future: Future[_T], callback: Callable[[_T],
                                                       _K]) -> Future[_K]:
    """Binds a callback to the future and provides a new one with callback result."""
    result: Future[_K] = Future()
    
    # called by the provided future
    def handler(_: Any) -> None:
        if result.cancelled():
            return
        if future.cancelled():
            result.cancel()
            return
        exp = future.exception()
        if exp is not None:
            result.set_exception(exp)
            return

        if not result.set_running_or_notify_cancel():
            return

        try:
            res = callback(future.result())
            result.set_result(res)
        except Exception as excep:
            result.set_exception(excep)

    # called by the returned future
    def cancel_handler(_: Any) -> None:
        if not result.cancelled():
            return
        future.cancel()

    future.add_done_callback(handler)
    result.add_done_callback(cancel_handler)
    return result

ZEPHYR_SHELL_PATTERN = "uart:~$ "


class SerialConnection:
    """Wrapps a pyserial connection and provides non-blocking interface for
    communication. The serial connection is handled in sub-thread and all API
    just interfaces with the thread."""

    def __init__(self, port: str, serial_builder) -> None:
        self.logger = logging.getLogger(
            f"{self.__class__.__module__}.{self.__class__.__name__}:{port}")
        self.serial_builder = serial_builder
        self.port = port
        self.ser = self.serial_builder(port)
        self.executor: ThreadPoolExecutor = ThreadPoolExecutor(max_workers=1)
        self.logger.info("SerialConnection for %s with baudrate %s",
                         self.ser.port, self.ser.baudrate)
        
        # TEMP Read all buffered bytes from the serial port
        if(callable(getattr(self.ser, 'read', None))):
            self.ser.read(2048)

    def transmit(self, data: str) -> Future[None]:
        """Sends data over the serial without blocking"""
        future = self._submit(lambda: self._transmit_impl(data))
        return future

    def receive_until(self, delimiter: str) -> Future[str]:
        """Receives data from the serial until `delimiter` is found, returns a
        `future` immediatly that will contain the data once it arrives."""

        future = self._submit(lambda: self._receive_until_impl(delimiter))
        return future

    def close(self) -> Future[None]:
        """Closes the internal serial connection"""

        def callback() -> None:
            self.logger.info("Closing port %s", self.port)
            self.ser.close()

        future = self._submit(callback)
        return future

    def connect(self) -> Future[None]:
        """Creates an internal serial connection"""

        def callback() -> None:
            self.logger.info("Connecting to port %s", self.port)
            self.ser = self.serial_builder(self.port)

        future = self._submit(callback)
        return future

    def reconnect(self) -> None:
        """Disconnects the serial connection and connects again"""
        self.close()
        self.connect()

    def reset_input_buffer(self) -> Future[None]:
        """Flushes the input buffer"""

        def callback() -> None:
            self.logger.info("Flushing input buffer of %s", self.port)
            self.ser.reset_input_buffer()

        future = self._submit(callback)
        return future

    def _submit(self, job: Callable[[], Any]) -> Future[Any]:

        def callback() -> Any:
            try:
                return job()
            except Exception as excep:
                self.logger.error(
                    "SerialConnection encountered exception in thread: %s",
                    excep)
                raise excep

        return self.executor.submit(callback)

    # Called by the transmit in separate thread
    def _transmit_impl(self, data: str) -> None:
        """Sends data over serial"""
        self._debug_log_data("w: %s", data)
        self.ser.write(data.encode("utf-8"))

    # Called by the receuive_until in separate thread
    def _receive_until_impl(self, delimiter: str) -> str:
        """Receives data from serial until delimiter is found"""
        tmp: bytes = self.ser.read_until(delimiter.encode("utf-8"))
        res: str = tmp.decode("utf-8")
        res = res[:-len(delimiter)]
        self._debug_log_data("r: %s", res)

        return res

    def _debug_log_data(self, fmt: str, data: str) -> None:
        self.logger.debug(fmt, repr(data)[1:-1])


class ZephyrShell:
    """Wraps a pyserial connection and provides a non-blocking interface to
    zephyrs shell. The communication happens in separate thread, the entire
    abstraction is non-blocking."""

    def __init__(self, port, ser_fac) -> None:
        self.logger = logging.getLogger(
            f"{self.__class__.__module__}.{self.__class__.__name__}")
        self.serial_conn: SerialConnection = SerialConnection(port, ser_fac)

    def port(self) -> str:
        return self.serial_conn.port

    def transmit(self, data: str) -> Future[None]:
        """Sends message to the console"""
        return self.serial_conn.transmit(data)

    def receive(self) -> Future[str]:
        """Receives an output from zephyr console, gets everything until next
        prompt"""
        future = self.serial_conn.receive_until(delimiter=ZEPHYR_SHELL_PATTERN)

        def callback(msg: str) -> str:
            return msg.strip()

        return map_future(future, callback)

    def exchange(self, data: str) -> Future[str]:
        self.transmit(data + "\n")
        return self.receive()

    def reset_input_buffer(self) -> Future[None]:
        """Flushes the input buffer"""
        return self.serial_conn.reset_input_buffer()

    def close(self) -> Future[None]:
        """Closes the internal serial connection"""
        return self.serial_conn.close()

    def connect(self) -> Future[None]:
        return self.serial_conn.connect()

    def reconnect(self) -> None:
        self.serial_conn.reconnect()
        
## Code from company codebase ends here
## The following code is my own impementation

class DTFWInfo:
    version: str = ''
    firmware_hash: str = ''
    mcuboot_version: int
    mcuboot_slot: int

class DTFWInfoShell:
    shell_command = "dt_fwinfo"

    def __init__(self, shell: ZephyrShell):
        self.shell = shell

    def _parse_line(self, line: str, fwinfo: DTFWInfo):
        re_fw_version = re.compile(r'firmware version: (\d+.\d+.\d+)')
        re_fw_hash = re.compile(r'firmware hash: (\w+)')
        re_mcuboot_version = re.compile(r'mcuboot version: (\d+)')
        re_mcuboot_slot = re.compile(r'mcuboot slot: (\d+)')

        if m := re_fw_version.search(line):
            fwinfo.version = m.group(1)
        elif m := re_fw_hash.search(line):
            fwinfo.firmware_hash = m.group(1)
        elif m := re_mcuboot_version.search(line):
            fwinfo.mcuboot_version = int(m.group(1))
        elif m := re_mcuboot_slot.search(line):
            fwinfo.mcuboot_slot = int(m.group(1))

    def get(self) -> DTFWInfo:
        self.shell.connect().result()
        self.shell.reset_input_buffer().result()
        shell_res = self.shell.exchange(self.shell_command).result().split("\n")
        self.shell.close().result()

        fwinfo = DTFWInfo()

        for line in shell_res:
            self._parse_line(line, fwinfo)

        return fwinfo

    def verify(self, expected: DTFWInfo):
        fwinfo = self.get()

        assert fwinfo.firmware_hash == expected.firmware_hash
        assert fwinfo.mcuboot_version == expected.mcuboot_version
        assert fwinfo.mcuboot_slot == expected.mcuboot_slot
