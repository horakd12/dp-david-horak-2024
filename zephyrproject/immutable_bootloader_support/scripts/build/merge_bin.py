#!/usr/bin/env python3

import argparse
import re
from intelhex import IntelHex

def run(input, output):
    if(len(input) == 0):
         return
    
    ih = IntelHex()

    for i in range(0, len(input), 2):
        ih.loadbin(input[i+1],offset=int(input[i], 0))

    ih.tobinfile(output)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", nargs='*', help='<address> <bin_file> <address> <bin_file> ...')
    parser.add_argument("-o", "--output", required=True)
    args = parser.parse_args()

    run(args.input, args.output)