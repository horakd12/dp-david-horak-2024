#!/usr/bin/env python3

import argparse
import re

def run(address_file, runner_file):
    address = None
    with open (address_file, 'r' ) as f:
        address = f.read().strip()

    content_new = None
    with open (runner_file, 'r' ) as f:
        content = f.read()
        content_new = re.sub(r'esp-app-address=0x\d*', "esp-app-address={}".format(address), content)

    with open (runner_file, 'w' ) as f:
        f.write(content_new)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--runner_file")
    parser.add_argument("--address_file")
    args = parser.parse_args()
    run(args.address_file, args.runner_file)