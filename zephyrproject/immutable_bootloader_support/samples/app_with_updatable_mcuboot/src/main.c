/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 * Copyright (c) 2020 Prevas A/S
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(main);

int main(void)
{
	LOG_INF("build time: " __DATE__ " " __TIME__);

	while (1) {
		k_sleep(K_MSEC(1000));
	}
	
	return 0;
}
