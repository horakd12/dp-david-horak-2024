function(esp32_update_runner)
    set(cmd_output ${PROJECT_BINARY_DIR}/runner_update.stamp)

    add_custom_command(
        OUTPUT ${cmd_output}
        COMMAND
            ${PYTHON_EXECUTABLE}
            ${ZEPHYR_IMMUTABLE_BOOTLOADER_SUPPORT_MODULE_DIR}/scripts/build/esp32_update_runner.py
            --runner_file ${PROJECT_BINARY_DIR}/runners.yaml
            --address_file ${PROJECT_BINARY_DIR}/partition_addr.txt
        COMMAND
            ${CMAKE_COMMAND} -E touch ${cmd_output}
        DEPENDS 
            ${PROJECT_BINARY_DIR}/runners.yaml
            ${PROJECT_BINARY_DIR}/partition_addr.txt
        )

    add_custom_target(esp32_update_runner_target ALL DEPENDS ${cmd_output})
    list(APPEND RUNNERS_DEPS esp32_update_runner_target)
endfunction()

esp32_update_runner()