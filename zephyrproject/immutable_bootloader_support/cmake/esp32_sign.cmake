if(NOT CONFIG_SOC_FAMILY_ESP32)
  return()
endif()

function(esp32_set_bin_file type path)
  set_target_properties(runners_yaml_props_target PROPERTIES "${type}_file" "${path}")
  # ESP32_BIN_TO_MERGE variable is used when merging .bin files
  set(ESP32_BIN_TO_MERGE ${path} CACHE STRING "Name of binary file which will be merged to one binary file" FORCE)
endfunction()

function(esp32_sign_by_espsecure INFILE OUTFILE RES)
  set(${RES} 0 PARENT_SCOPE)

  if(NOT CONFIG_SIGN_BY_ESPSECURE)
    message(WARNING "CONFIG_SIGN_BY_ESPSECURE is set to \"n\", image will not be signed by espsecure.py.")
    return()
  endif()

  set(keyfile "${CONFIG_ESPSECURE_SIGNATURE_KEY}")

  if("${keyfile}" STREQUAL "")
    message(FATAL_ERROR "Cannot sign image using espsecure.py, CONFIG_ESPSECURE_SIGNATURE_KEY is not set!")
  endif()

  if(NOT IS_ABSOLUTE "${keyfile}")
    set(keyfile "${WEST_TOPDIR}/${keyfile}")
  endif()

  message("ESPSECURE key file: ${keyfile}")

  set(align "")
  if(CONFIG_ESPSECURE_NO_ALIGN)
    set(align "--no-align")
  endif()

  set_property(GLOBAL APPEND PROPERTY extra_post_build_commands
    COMMAND 
      ${PYTHON_EXECUTABLE} ${ESP_IDF_PATH}/components/esptool_py/esptool/espsecure.py
      sign_data
      --version 2 
      ${align}
      --keyfile ${keyfile}
      --output ${OUTFILE}
      ${INFILE}
    COMMAND 
      test $$\( stat -c%s "${OUTFILE}" \) -lt $$\(\($$\(cat ${ZEPHYR_BINARY_DIR}/partition_size.txt\)\)\)
    )

  set_property(GLOBAL APPEND PROPERTY extra_post_build_byproducts ${OUTFILE})

  esp32_set_bin_file(bin ${OUTFILE})

  set(${RES} 1 PARENT_SCOPE)
endfunction()


function(esp32_sign_by_imgtool INFILE OUTFILE RES)
  set(${RES} 0 PARENT_SCOPE)

  if(CONFIG_MCUBOOT)
    set(keyfile "${CONFIG_BOOT_SIGNATURE_KEY_FILE}")
  elseif(CONFIG_BOOTLOADER_MCUBOOT)
    set(keyfile "${CONFIG_MCUBOOT_SIGNATURE_KEY_FILE}")
  else()
    set(keyfile "")
  endif()

  if(NOT "${keyfile}" STREQUAL "")
    if(NOT IS_ABSOLUTE "${keyfile}")
      set(keyfile "${WEST_TOPDIR}/${keyfile}")
    endif()
  endif()

  if(IMGTOOL)
    set(imgtool_path "${IMGTOOL}")
  elseif(DEFINED ZEPHYR_MCUBOOT_MODULE_DIR)
    set(IMGTOOL_PY "${ZEPHYR_MCUBOOT_MODULE_DIR}/scripts/imgtool.py")
    if(EXISTS "${IMGTOOL_PY}")
      set(imgtool_path "${IMGTOOL_PY}")
    endif()
  endif()

  set(key_arg "")

  if(NOT "${keyfile}" STREQUAL "")
      set(key_arg --key "${keyfile}")
  endif()

  message("Signing by imgtool, key: ${keyfile}")
  message("key arg: ${key_arg}")
  
  if(CONFIG_BOOTLOADER_MCUBOOT)
    set(imgtool_version ${CONFIG_MCUBOOT_IMGTOOL_SIGN_VERSION})
  elseif(CONFIG_BOOTLOADER_IMMUTABLE_ESP32)
    set(imgtool_version ${CONFIG_DT_ESP32_IMG_INFO_VERSION})
  else()
    message(WARNING "IMGTOOL version is not set!")
    set(imgtool_version "0.0.0")
  endif()

  set(sign_cmd
    ${PYTHON_EXECUTABLE} ${imgtool_path}
    sign
    "${key_arg}"
    --align 4
    --version ${imgtool_version}
    --header-size 0x20
    --slot-size $$\(cat ${ZEPHYR_BINARY_DIR}/partition_size.txt\) 
    --pad-header
    --erased-val 0xff
    ${INFILE}
    ${OUTFILE}
    )
    
  set_property(GLOBAL APPEND PROPERTY extra_post_build_commands COMMAND ${sign_cmd})
  set_property(GLOBAL APPEND PROPERTY extra_post_build_byproducts ${OUTFILE})

  esp32_set_bin_file(bin ${OUTFILE})

  set(${RES} 1 PARENT_SCOPE)
endfunction()

set(input_file ${ZEPHYR_BINARY_DIR}/${KERNEL_NAME}.bin)
set(output_file ${ZEPHYR_BINARY_DIR}/${KERNEL_NAME}.signed.bin)

esp32_set_bin_file(bin ${input_file})

if(CONFIG_BOOT_IMMUTABLE_ESP32)
  esp32_sign_by_espsecure(${input_file} ${output_file} res)
elseif(CONFIG_BOOTLOADER_IMMUTABLE_ESP32)
  set(tmp_file ${ZEPHYR_BINARY_DIR}/tmp.bin)
  esp32_sign_by_espsecure(${input_file} ${tmp_file} res)

  if(NOT ${res})
    set(tmp_file ${input_file})
  endif()

  esp32_sign_by_imgtool(${tmp_file} ${output_file} res)
elseif(CONFIG_BOOTLOADER_MCUBOOT)
  esp32_sign_by_imgtool(${input_file} ${output_file} res)
endif()