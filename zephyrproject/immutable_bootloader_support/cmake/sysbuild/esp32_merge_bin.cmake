function(esp32_merge_bin images)
    set(merged_bin ${APPLICATION_BINARY_DIR}/merged.bin)

    set(merge_args "")
    set(merge_deps "")

    set(merge 0)

    foreach(image ${images})
        sysbuild_get(${image}_CONFIG_SOC_FAMILY_ESP32 IMAGE ${image} VAR CONFIG_SOC_FAMILY_ESP32 KCONFIG)
        sysbuild_get(${image}_CONFIG_KERNEL_BIN_NAME IMAGE ${image} VAR CONFIG_KERNEL_BIN_NAME KCONFIG)

        if(${image}_CONFIG_SOC_FAMILY_ESP32)
            set(merge 1)

            # ESP32_BIN_TO_MERGE is set when signing images
            set(BIN_TO_MERGE "")
            sysbuild_get(BIN_TO_MERGE IMAGE ${image} VAR ESP32_BIN_TO_MERGE CACHE)

            if("${BIN_TO_MERGE}" STREQUAL "")
                set(BIN_TO_MERGE "${APPLICATION_BINARY_DIR}/${image}/zephyr/${${image}_CONFIG_KERNEL_BIN_NAME}.bin")
            endif()
            
            set(merge_args
                ${merge_args} 
                $$\(cat ${APPLICATION_BINARY_DIR}/${image}/zephyr/partition_addr.txt\) 
                ${BIN_TO_MERGE}
            )
            
            list(APPEND merge_deps ${APPLICATION_BINARY_DIR}/${image}/zephyr/partition_addr.txt)
            list(APPEND merge_deps ${APPLICATION_BINARY_DIR}/${image}/zephyr/${${image}_CONFIG_KERNEL_BIN_NAME}.bin)
        endif()
    endforeach()

    if(${merge})
        add_custom_command(
            OUTPUT ${merged_bin}
            COMMAND
            ${PYTHON_EXECUTABLE}
            ${ZEPHYR_IMMUTABLE_BOOTLOADER_SUPPORT_MODULE_DIR}/scripts/build/merge_bin.py
            ${merge_args}
            -o ${merged_bin}
            DEPENDS ${merge_deps}
            )
        add_custom_target(esp32_merge_bin_target ALL DEPENDS ${merged_bin})
    endif()
endfunction()