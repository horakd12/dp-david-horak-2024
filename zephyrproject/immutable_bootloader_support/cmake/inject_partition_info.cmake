function(inject_partition_info)
    if(CONFIG_BOOT_IMMUTABLE_ESP32)
        dt_nodelabel(dts_partition_path NODELABEL "boot_partition")
        dt_reg_addr(offset PATH ${dts_partition_path})
        dt_reg_size(size PATH ${dts_partition_path})
    elseif(CONFIG_BOOTLOADER_IMMUTABLE_ESP32_S1_VARIANT)
        dt_nodelabel(dts_partition_path NODELABEL "s1")
        dt_reg_addr(offset PATH ${dts_partition_path})
        dt_reg_size(size PATH ${dts_partition_path})
    elseif(CONFIG_BOOTLOADER_IMMUTABLE_ESP32)
        dt_nodelabel(dts_partition_path NODELABEL "s0")
        dt_reg_addr(offset PATH ${dts_partition_path})
        dt_reg_size(size PATH ${dts_partition_path})
    elseif(CONFIG_MCUBOOT)
        dt_nodelabel(dts_partition_path NODELABEL "boot_partition")
        dt_reg_addr(offset PATH ${dts_partition_path})
        dt_reg_size(size PATH ${dts_partition_path})
    else()
        dt_nodelabel(dts_partition_path NODELABEL "slot0_partition")
        dt_reg_addr(offset PATH ${dts_partition_path})
        dt_reg_size(size PATH ${dts_partition_path})
    endif()

    file(
    GENERATE OUTPUT ${PROJECT_BINARY_DIR}/partition_size.txt
    CONTENT ${size}
    )
    
    file(GENERATE OUTPUT ${PROJECT_BINARY_DIR}/partition_addr.txt
    CONTENT ${offset}
    )
endfunction()

inject_partition_info()