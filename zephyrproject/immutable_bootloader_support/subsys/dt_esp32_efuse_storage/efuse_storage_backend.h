#ifndef DT_ESP32_EFUSE_STORAGE_BACKEND_H
#define DT_ESP32_EFUSE_STORAGE_BACKEND_H

#include <stdint.h>
#include <dronetag/dt_esp32_efuse_storage.h>

#include "esp_efuse.h"

int efuse_storage_backend_read_blob(const esp_efuse_desc_t* field[], void* data, int data_size_bits);
int efuse_storage_backend_write_blob(const esp_efuse_desc_t* field[], const void* data, int data_size_bits);

#endif /* DT_ESP32_EFUSE_STORAGE_BACKEND_H */