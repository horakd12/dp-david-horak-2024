#include <stdint.h>
#include <string.h>
#include <zephyr/kernel.h>
#include <zephyr/init.h>
#include <dronetag/dt_esp32_efuse_storage.h>
#include <zephyr/logging/log.h>
#include <esp_efuse_table.h>

#include "esp_efuse.h"
#include "efuse_storage_backend.h"

LOG_MODULE_REGISTER(efuse_storage_virtual_backend, CONFIG_DT_ESP32_EFUSE_STORAGE_LOG_LEVEL);

#define BLOCK_SIZE_BYTES (256 / 8)

int backend_virtual_read_block(int block_num, void* data, int size);
int backend_virtual_write_block(int block_num, void* data, int size);
int backend_virtual_init();

void efuse_storage_backend_print_content(){
    LOG_DBG("efuse content in flash:");

    uint8_t block[BLOCK_SIZE_BYTES];
    memset(block, 0, sizeof(block));

    for(int i = 0; i < EFUSE_BLK_MAX; i++){
        backend_virtual_read_block(i, block, sizeof(block));

        LOG_DBG("block %d in flash:", i);
        LOG_HEXDUMP_DBG(block, sizeof(block), "block data:");
    }
}

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_IMMUTABLE_KEY0
#if CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY0_BLOCK < 0
#error "Block of immutable key 0 is not specified, set CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY0_BLOCK option "
#endif
extern const unsigned char esp_key_digest_0[32];
#endif

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_IMMUTABLE_KEY1
#if CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY1_BLOCK < 0
#error "Block of immutable key 1 is not specified, set CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY1_BLOCK option "
#endif
extern const unsigned char esp_key_digest_1[32];
#endif

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_IMMUTABLE_KEY2
#if CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY2_BLOCK < 0
#error "Block of immutable key 2 is not specified, set CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY2_BLOCK option "
#endif
extern const unsigned char esp_key_digest_2[32];
#endif

static void efuse_storage_write_virtual_data(){
    LOG_INF("Writing data to virtual eFuse");
#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_SECURE_BOOT_EN
    uint8_t data = 1;
    int res = dt_esp32_efuse_write_blob(ESP_EFUSE_SECURE_BOOT_EN, &data, 1);
#endif

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_IMMUTABLE_KEY0
    {
        esp_efuse_desc_t desc = { CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY0_BLOCK, 0, 256 };
        const esp_efuse_desc_t* field[] = { &desc, NULL};
        dt_esp32_efuse_write_blob(field, esp_key_digest_0, 256);
    }
#endif

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_IMMUTABLE_KEY1
    {
        esp_efuse_desc_t desc = { CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY1_BLOCK, 0, 256 };
        const esp_efuse_desc_t* field[] = { &desc, NULL};
        dt_esp32_efuse_write_blob(field, esp_key_digest_1, 256);
    }
#endif

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_VIRTUAL_IMMUTABLE_KEY2
    {
        esp_efuse_desc_t desc = { CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY2_BLOCK, 0, 256 };
        const esp_efuse_desc_t* field[] = { &desc, NULL};
        dt_esp32_efuse_write_blob(field, esp_key_digest_2, 256);
    }
#endif
}

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_BACKEND_VIRTUAL_LOAD_EFUSE

#include <esp_efuse.h>
#include <esp_efuse_utility.h>

extern const esp_efuse_range_addr_t range_read_addr_blocks[];

static int virtual_backend_load_efuse(){
    int err = 0;

    LOG_INF("loading physical efuses");

    static uint8_t block[BLOCK_SIZE_BYTES];

    for(int i = 0; i < EFUSE_BLK_MAX; i++){
        const esp_efuse_desc_t efuse_desc = {i, 0, 256};
        const esp_efuse_desc_t* desc_arr[] = {&efuse_desc, NULL};

        uint8_t bytes_to_read = range_read_addr_blocks[i].end - range_read_addr_blocks[i].start + sizeof(uint32_t);

        memset(block, 0, sizeof(block));
        err = esp_efuse_read_field_blob(desc_arr, block, bytes_to_read * 8);
        if(err != 0){
            return err;
        }

        backend_virtual_write_block(i, block, sizeof(block));
    }

    return 0;
}
#endif


#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_BACKEND_VIRTUAL_FLASH
#include <zephyr/drivers/flash.h>
#include <zephyr/storage/flash_map.h>
#include <zephyr/fs/nvs.h>

#define PARTITION_NAME efuse_storage
#define EFUSE_STORAGE_PARTITION_ID FIXED_PARTITION_ID(PARTITION_NAME)
#define STORAGE_INIT_KEY_ID 0xFFFF

static struct nvs_fs fs;

int backend_virtual_read_block(int block_num, void* data, int size){
    int ret = nvs_read(&fs, block_num, data, size);

    if(ret >= 0 && ret != size){
        return -EIO;
    }

    return ret;
}

int backend_virtual_write_block(int block_num, void* data, int size){
    int ret = nvs_write(&fs, block_num, data, size);

    if(ret > 0 && ret != size){
        return -EIO;
    }

    return ret;
}

int backend_virtual_init(){
    int err = 0;

    LOG_INF("initializing flash backend");

    const struct flash_area* fa;
    err = flash_area_open(EFUSE_STORAGE_PARTITION_ID, &fa);
    if(err != 0) {
        LOG_ERR("cannot open flash area: %d", err);
        return -ENODEV;
    }

    struct flash_pages_info info;

    fs.flash_device = fa->fa_dev;
    fs.offset = fa->fa_off;

    err = flash_get_page_info_by_offs(fs.flash_device, fs.offset, &info);

    fs.sector_size = info.size;
    fs.sector_count = fa->fa_size / info.size;

    if(fa->fa_off % info.size || fa->fa_size & info.size){
        LOG_ERR("flash area offset or size is not aligned to page size");
        return -EINVAL;
    }

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_BACKEND_VIRTUAL_FLASH_ERASE
    err = flash_area_erase(fa, 0, fa->fa_size);
    if(err != 0){
        LOG_ERR("failed erasing flash area");
        return err;
    }
#endif

    flash_area_close(fa);

    err = nvs_mount(&fs);
    if(err != 0){
        LOG_ERR("failed to mount nvs: %d", err);
        return err;
    }

    bool init = false;
    nvs_read(&fs, STORAGE_INIT_KEY_ID, &init, sizeof(init));

    if(init){
        efuse_storage_backend_print_content();
        return err;
    }

    LOG_INF("erasing flash content");

    nvs_delete(&fs, STORAGE_INIT_KEY_ID);

    static uint8_t block[BLOCK_SIZE_BYTES];
    memset(block, 0, sizeof(block));

    for(int i = 0; i < EFUSE_BLK_MAX; i++){
        nvs_write(&fs, i, block, sizeof(block));
    }

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_BACKEND_VIRTUAL_LOAD_EFUSE
    err = virtual_backend_load_efuse();
    if(err != 0){
        LOG_ERR("failed reading physical efuses");
        return err;
    }
#endif

    init = 1;
    nvs_write(&fs, STORAGE_INIT_KEY_ID, &init, sizeof(init));

    efuse_storage_write_virtual_data();

    efuse_storage_backend_print_content();

    if(err == 0){
        LOG_INF("backend successfully initialized");
    }

    return err;
}

#endif

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_BACKEND_VIRTUAL_RAM

static uint8_t efuse_storage[BLOCK_SIZE_BYTES * EFUSE_BLK_MAX];

int backend_virtual_read_block(int block_num, void* data, int size){
    memcpy(data, (uint8_t*)efuse_storage + (block_num * BLOCK_SIZE_BYTES), size);
    return 0;
}

int backend_virtual_write_block(int block_num, void* data, int size){
    memcpy((uint8_t*)efuse_storage + (block_num * BLOCK_SIZE_BYTES), data, size);
    return 0;
}

int backend_virtual_init(){
    int err = 0;

    LOG_INF("initializing virtual backend in ram");

    memset(efuse_storage, 0, BLOCK_SIZE_BYTES * EFUSE_BLK_MAX);

#ifdef CONFIG_DT_ESP32_EFUSE_STORAGE_BACKEND_VIRTUAL_LOAD_EFUSE
    err = virtual_backend_load_efuse();
    if(err != 0){
        LOG_ERR("failed reading physical efuses");
        return err;
    }
#endif

    if(err == 0){
        LOG_INF("backend successfully initialized");
    }

    efuse_storage_write_virtual_data();

    efuse_storage_backend_print_content();

    return err;
}

#endif


static uint8_t mask_right(uint8_t byte, uint8_t num_bits_left){
    if(num_bits_left == 0) return 0;

    uint8_t m = 1;

    for(int i = 1; i < num_bits_left; i++){
        m <<= 1;
        m |= 1;
    }

    return byte & m;
}

static void shift_left(void* data, int size){
    uint8_t bit = 0;
    for(int i = 0; i < size; i++){
        uint8_t b = ((uint8_t*)data)[i] & 0x80;

        ((uint8_t*)data)[i] <<= 1;

        if(bit){
            ((uint8_t*)data)[i] |= 0x01;
        }

        bit = b;
    }
}


static void shift_right(void* data, int size){
    uint8_t bit = 0;
    for(int i = size - 1; i >= 0; i--){
        uint8_t b = ((uint8_t*)data)[i] & 0x01;

        ((uint8_t*)data)[i] >>= 1;

        if(bit == 1){
            ((uint8_t*)data)[i] |= 0x80;
        }

        bit = b;
    }
}

static int read_field(const esp_efuse_desc_t* field, void* data, int data_size_bits){
    if(field->bit_start + field->efuse_block > 256){
        return -EINVAL;
    }

    memset(data, 0, data_size_bits / 8);
    if(data_size_bits % 8 != 0){
        ((uint8_t*)data)[data_size_bits / 8] &= ~mask_right(0xFF, data_size_bits % 8);
    }

    static uint8_t block[BLOCK_SIZE_BYTES];
    backend_virtual_read_block(field->efuse_block, block, sizeof(block));

    for(int i = 0; i < field->bit_start; i++){
        shift_right(block, sizeof(block));
    }

    int size_bits = MIN(field->bit_count, data_size_bits);

    int data_idx = 0;
    while(size_bits > 0){
        if(size_bits < 8){
            ((uint8_t*)data)[data_idx] |= mask_right(block[data_idx], size_bits);
            size_bits = 0;
            break;
        }

        ((uint8_t*)data)[data_idx] = block[data_idx];

        size_bits -= 8;
        data_idx++;
    }

    return 0;
}

static int write_field(const esp_efuse_desc_t* field, void* data, int data_size_bits){
    if(field->bit_start + field->bit_count > 256){
        return -EINVAL;
    }

    static uint8_t block[BLOCK_SIZE_BYTES];

    backend_virtual_read_block(field->efuse_block, block, sizeof(block));

    int size_bits = MIN(field->bit_count, data_size_bits);

    uint8_t block_data[BLOCK_SIZE_BYTES];
    memset(block_data, 0, sizeof(block_data));

    int data_idx = 0;
    while(size_bits > 0){
        if(size_bits < 8){
            block_data[data_idx] = mask_right(((uint8_t*)data)[data_idx], size_bits);
            size_bits = 0;
            break;
        }

        block_data[data_idx] = ((uint8_t*)data)[data_idx];

        size_bits -= 8;
        data_idx++;
    }

    for(int i = 0; i < field->bit_start; i++){
        shift_left(block_data, sizeof(block_data));
    }

    for(int i = 0; i < BLOCK_SIZE_BYTES; i++){
        block[i] |= block_data[i];
    }

    backend_virtual_write_block(field->efuse_block, block, sizeof(block));

    return 0;
}

int efuse_storage_backend_read_blob(const esp_efuse_desc_t* field[], void* data, int data_size_bits){
    int bit_size = 0;
    int fields_num = 0;
    int read_bits = 0;
    while(field[fields_num] != NULL){
        bit_size += field[fields_num]->bit_count;

        if(bit_size > data_size_bits){
            read_bits = field[fields_num]->bit_count - (bit_size - data_size_bits);
            fields_num++;
            break;
        } else {
            read_bits = field[fields_num]->bit_count;
        }

        fields_num++;
    }

    if(fields_num < 1){
        return -EINVAL;
    }

    int idx = fields_num - 1;
    int shift = 0;
    while(idx >= 0){
        for(int j = 0; j < shift; j++){
            shift_left(data, (data_size_bits + 7) / 8);
        }

        read_field(field[idx], data, read_bits);

        idx--;

        if(idx >= 0){
            shift = field[idx]->bit_count;
            read_bits = field[idx]->bit_count;
        }
    }

    return 0;
}

int efuse_storage_backend_write_blob(const esp_efuse_desc_t* field[], const void* data, int data_size_bits){
    int bit_size = 0;
    int fields_num = 0;
    while(field[fields_num] != NULL){
        bit_size += field[fields_num]->bit_count;
        fields_num++;
    }

    if(data_size_bits > bit_size || fields_num < 1){
        return -EINVAL;
    }


    static uint8_t buff[BLOCK_SIZE_BYTES * EFUSE_BLK_MAX];
    memset(buff, 0, sizeof(buff));
    memcpy(buff, data, (data_size_bits + 7) / 8);


    int idx = 0;
    int shift = 0;
    int remaining_bits = data_size_bits;
    while(field[idx] != NULL && remaining_bits > 0){
        for(int i = 0; i < shift; i++){
            shift_right(buff, (data_size_bits + 7) / 8);
        }

        int write_bits = MIN(field[idx]->bit_count, remaining_bits);

        write_field(field[idx], buff, write_bits);

        shift = write_bits;
        remaining_bits -= write_bits;
        idx++;
    }
    
    return 0;
}

SYS_INIT(backend_virtual_init, POST_KERNEL, 60);