#include <string.h>
#include <zephyr/logging/log.h>
#include <dronetag/dt_esp32_efuse_storage.h>

#include "esp_efuse.h"
#include "efuse_storage_backend.h"

LOG_MODULE_REGISTER(efuse_storage, CONFIG_DT_ESP32_EFUSE_STORAGE_LOG_LEVEL);

int dt_esp32_efuse_read_blob(const esp_efuse_desc_t* field[], void* data, int data_size_bits){
    int err = efuse_storage_backend_read_blob(field, data, data_size_bits);

    LOG_HEXDUMP_DBG(data, (data_size_bits + 7) / 8, "read data:");

    return err;
}

int dt_esp32_efuse_write_blob(const esp_efuse_desc_t* field[], const void* data, int data_size_bits){
    return efuse_storage_backend_write_blob(field, data, data_size_bits);
}

static const esp_efuse_desc_t key_fields[] = {
#if CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY0_BLOCK > 0
    {CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY0_BLOCK, 0, 256},
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY1_BLOCK > 0
    {CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY1_BLOCK, 0, 256},
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY2_BLOCK > 0
    {CONFIG_DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEY2_BLOCK, 0, 256},
#endif
};

int dt_esp32_efuse_get_immutable_keys(dt_esp32_efuse_keys_t* keys){
    int err;

    uint8_t key[32] = {0};

    keys->key_num = 0;

    for(int i = 0; i < ARRAY_SIZE(key_fields); i++){
        if(key_fields[i].efuse_block >= EFUSE_BLK_MAX){
            continue;
        }

        const esp_efuse_desc_t* field[] = { &key_fields[i], NULL};

        err = dt_esp32_efuse_read_blob(field, key, sizeof(key) * 8);
        memcpy(&keys->key[i], key, 32);
        keys->key_num += 1;
    }

    return 0;
}

#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA0_BLOCK > 0
static const esp_efuse_desc_t user_data_0 = {CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA0_BLOCK, 0, 256};
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA1_BLOCK > 0
static const esp_efuse_desc_t user_data_1 = {CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA1_BLOCK, 0, 256};
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA2_BLOCK > 0
static const esp_efuse_desc_t user_data_2 = {CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA2_BLOCK, 0, 256};
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA3_BLOCK > 0
static const esp_efuse_desc_t user_data_3 = {CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA3_BLOCK, 0, 256};
#endif

static const esp_efuse_desc_t* user_data_desc[] = {
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA0_BLOCK > 0
    &user_data_0,
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA1_BLOCK > 0
    &user_data_1,
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA2_BLOCK > 0
    &user_data_2,
#endif
#if CONFIG_DT_ESP32_EFUSE_STORAGE_USER_DATA3_BLOCK > 0
    &user_data_3,
#endif
    NULL
};

static const int user_data_size = (ARRAY_SIZE(user_data_desc) - 1) * 32;

int dt_esp32_efuse_read_user_data(void* data, int size){
    if(size > user_data_size){
        return -EINVAL;
    }

    return dt_esp32_efuse_read_blob(user_data_desc, data, size * 8);
}

int dt_esp32_efuse_write_user_data(void* data, int size){
    if(size > user_data_size){
        return -EINVAL;
    }
    return dt_esp32_efuse_write_blob(user_data_desc, data, size * 8);
}

static const esp_efuse_desc_t counter_field = {
#if CONFIG_DT_ESP32_EFUSE_STORAGE_COUNTER_BLOCK > 0
    {CONFIG_DT_ESP32_EFUSE_STORAGE_COUNTER_BLOCK, 0, 256},
#endif
};

int dt_esp32_efuse_get_counter_val(int* val){
    return 0;
}

int dt_esp32_efuse_set_counter_val(int val){
    return 0;
}