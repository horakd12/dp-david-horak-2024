#include <dronetag/dt_esp32_efuse_storage.h>
#include "efuse_storage_backend.h"
#include "esp_efuse.h"

int efuse_storage_backend_read_blob(const esp_efuse_desc_t* field[], void* data, int data_size_bits){
    return esp_efuse_read_field_blob(field, data, data_size_bits);
}

int efuse_storage_backend_write_blob(const esp_efuse_desc_t* field[], const void* data, int data_size_bits){
    return esp_efuse_write_field_blob(field, data, data_size_bits);
}