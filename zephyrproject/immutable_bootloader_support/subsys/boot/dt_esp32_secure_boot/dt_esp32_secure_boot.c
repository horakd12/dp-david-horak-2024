#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>

#include "esp_efuse.h"
#include "esp_efuse_table.h"

#include "dt_esp32_sb.h"

#include <zephyr/logging/log.h>
#include <zephyr/storage/flash_map.h>

#include <dronetag/dt_esp32_efuse_storage.h>

LOG_MODULE_REGISTER(dt_esp32_secure_boot, CONFIG_DT_ESP32_SECURE_BOOT_LOG_LEVEL);

static bool secure_boot_enabled(){
    uint8_t enabled = 0;
    int res = dt_esp32_efuse_read_blob(ESP_EFUSE_SECURE_BOOT_EN, &enabled, sizeof(enabled) * 8);
    if(res != 0){
        LOG_ERR("failed reading SECURE_BOOT_EN bit");
        abort();
    }

    return enabled != 0;
}

static int check_signature_block(dt_esp32_sb_signature_block_t* signature){
    const uint32_t crc_sig_block_len = offsetof(dt_esp32_sb_signature_block_t, block_crc);
    
    uint32_t signature_block_crc = dt_esp32_sb_crc32(0, (uint8_t*)signature, crc_sig_block_len);

    if(signature->magic_byte != DT_ESP32_SB_SIGNATURE_MAGIC){
        return -EINVAL;
    }

    if(signature->block_crc != signature_block_crc){
        return -EINVAL;
    }

    return 0;
}

static int get_image_digest(uint8_t fa_id, uint32_t offset, uint32_t size, uint8_t* digest){
    int err = ~0;

    if(digest == NULL){
        return -EINVAL;
    }

    static uint8_t buffer[8192];

    void* handle = dt_esp32_sb_sha256_init();

    uint32_t image_loc = 0;
    uint32_t remaining_size = size;

    static const struct flash_area* slot;
    err = flash_area_open(fa_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", fa_id);
        abort();
    }
    
    do {
        uint32_t read = MIN(remaining_size, sizeof(buffer));
        
        err = flash_area_read(slot, offset + image_loc, buffer, read);
        
        if(err != 0){
            err = err == -EINVAL ? -EINVAL : -EIO;
            goto end;
        }

        dt_esp32_sb_sha256_update(handle, buffer, read);

        image_loc += read;
        remaining_size -= read;
    } while (remaining_size > 0);

    dt_esp32_sb_sha256_finish(handle, digest);

end:
    flash_area_close(slot);
    return err;
}

static bool verify_public_key(dt_esp32_sb_rsa_pubkey_t* pubkey){
    uint8_t signature_key_digest[32] = {0};

    void* handle = dt_esp32_sb_sha256_init();
    dt_esp32_sb_sha256_update(handle, pubkey, sizeof(dt_esp32_sb_rsa_pubkey_t));
    dt_esp32_sb_sha256_finish(handle, signature_key_digest);

    int err = ~0;
    dt_esp32_efuse_keys_t trusted_keys;
    err = dt_esp32_efuse_get_immutable_keys(&trusted_keys);
    if(err != 0){
        LOG_ERR("reading trusted keys from efuse failed");
        abort();
    }

    for(int i = 0; i < trusted_keys.key_num; i++) {
        if(memcmp(signature_key_digest, trusted_keys.key[i], 32) == 0){
            return true;
        }
    }

    return false;
}


int dt_esp32_secure_boot_verify_image(uint8_t fa_id, uint32_t offset, uint32_t size){
    int err = ~0;

    if(!secure_boot_enabled()){
        err = 0;
        LOG_DBG("secure boot is disabled");
        goto end;
    }

    LOG_DBG("secure boot is enabled");

    static dt_esp32_sb_signature_block_t signature = {0};

    static const struct flash_area* slot;

    err = ~0;
    err = flash_area_open(fa_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", fa_id);
        abort();
    }

    err = ~0;
    err = flash_area_read(slot, offset + size, &signature, sizeof(signature));
    if(err != 0){
        LOG_ERR("failed reading signature block");
        err = err == -EINVAL ? -EINVAL : -EIO;
        goto end;
    }

    flash_area_close(slot);

    err = ~0;
    err = check_signature_block(&signature);
    if(err != 0){
        LOG_ERR("invalid signature block magic or crc");
        goto end;
    }

    bool valid_key = false;
    valid_key = verify_public_key(&signature.key);
    if(!valid_key) {
        LOG_ERR("image is signed with untrusted key");
        err = -EINVAL;
        goto end;
    }

    uint8_t image_digest[32];

    err = ~0;
    err = get_image_digest(fa_id, offset, size, image_digest);
    if(err != 0){
        LOG_ERR("cannot calculate image digest");
        goto end;
    }

    uint8_t verified_digest[32];

    bool verified = false;
    verified = dt_esp32_sb_rsa_pss_verify(&signature.key, (uint8_t*)&signature.signature, image_digest, verified_digest);
    
    if(!verified) {
        LOG_ERR("image verification failed");
        err = -EINVAL;
        goto end; 
    }

    LOG_INF("image successfuly verified");

end:
    return err;
}