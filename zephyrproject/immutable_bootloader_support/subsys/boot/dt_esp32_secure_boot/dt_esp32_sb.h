#ifndef DT_ESP32_SECURE_BOOT_INTERNAL_H
#define DT_ESP32_SECURE_BOOT_INTERNAL_H

#include <stdint.h>
#include <stdbool.h>

#define DT_ESP32_SB_SIGNATURE_MAGIC 0xE7

/** 
 * @brief ESP32 Public key
 */
typedef struct {
    uint8_t n[384];
    uint32_t e;
    uint8_t rinv[384];
    uint32_t mdash;
} dt_esp32_sb_rsa_pubkey_t;

/** 
 * @brief ESP32 signature block
 */
typedef struct {
    uint8_t magic_byte;
    uint8_t version;
    uint8_t _reserved1;
    uint8_t _reserved2;
    uint8_t image_digest[32];
    dt_esp32_sb_rsa_pubkey_t key;
    uint8_t signature[384];
    uint32_t block_crc;
    uint8_t _padding[16];
} dt_esp32_sb_signature_block_t;

_Static_assert(sizeof(dt_esp32_sb_signature_block_t) == 1216, "invalid sig block size");


/** 
 * @brief Calculate CRC32
 * 
 * @param crc   initial crc value
 * @param data  buffer containing data
 * @param len   size of buffer
 * 
 * @return calculated crc32
 */
uint32_t dt_esp32_sb_crc32(uint32_t crc, uint8_t *buf, uint32_t len);


/** 
 * @brief Initialize SHA256 module context
 * 
 * @return  hasher context
 */
void* dt_esp32_sb_sha256_init();

/** 
 * @brief Feeds data into hasher
 * 
 * @param handle   hasher context
 * @param data     data to be hashed
 * @param len      length of data
 * 
 * This can be called multiple times.
 */
void  dt_esp32_sb_sha256_update(void* handle, const void *data, uint32_t len);

/** 
 * @brief Finish SHA256 digest calculation
 * This function will free and returns calculated digest
 */
void  dt_esp32_sb_sha256_finish(void* handle, uint8_t* digest);

/** 
 * @brief Verify signature
 * 
 * This function verifies calculated image digest against signature block
 * 
 * @param key               public key used for verification
 * @param sig               pointer to signature structure
 * @param digest            calculated image digest
 * @param verified_digest   verified digest will be saved to this array
 * 
 * @retval true     If verification succeeded
 * 
 */
bool  dt_esp32_sb_rsa_pss_verify(dt_esp32_sb_rsa_pubkey_t *key, uint8_t *sig, uint8_t *digest, uint8_t *verified_digest);

#endif /* DT_ESP32_SECURE_BOOT_INTERNAL_H */