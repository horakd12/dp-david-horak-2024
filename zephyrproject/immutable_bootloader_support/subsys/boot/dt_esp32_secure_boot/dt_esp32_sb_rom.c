#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "dt_esp32_sb.h"

#include "crc.h"

uint32_t dt_esp32_sb_crc32(uint32_t crc, uint8_t* buf, uint32_t len){
    return crc32_le(crc, buf, len);
}

#include "sha.h"

static SHA_CTX ctx;

void* dt_esp32_sb_sha256_init(){
    ets_sha_enable();
    ets_sha_init(&ctx, SHA2_256);
    return &ctx;
}

void dt_esp32_sb_sha256_update(void* handle, const void *data, uint32_t len){
    assert(handle != NULL);
    ets_sha_update(handle, data, len, false);

    return;
}

void dt_esp32_sb_sha256_finish(void* handle, uint8_t* digest){
    assert(handle != NULL);

    if(digest != NULL){
        ets_sha_finish(handle, digest);
    }

    ets_sha_disable();

    memset((uint8_t*)&ctx, 0, sizeof(ctx));
    return;
}

#include "rsa_pss.h"

bool dt_esp32_sb_rsa_pss_verify(dt_esp32_sb_rsa_pubkey_t *key, uint8_t *sig, uint8_t *digest, uint8_t *verified_digest){
    bool valid = false;
    valid = ets_rsa_pss_verify((ets_rsa_pubkey_t*)key, sig, digest, verified_digest);
    return valid;
}