#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <zephyr/logging/log.h>

#include <soc/soc.h>

#include <dronetag/boot/dt_esp32_loader.h>

#include "boot/esp_mcuboot_image.h"

LOG_MODULE_REGISTER(dt_esp32_loader, CONFIG_DT_ESP32_LOADER_LOG_LEVEL);

#define IMAGE_RAM_ADDR DT_REG_ADDR(DT_CHOSEN(zephyr_sram))
#define IMAGE_RAM_SIZE DT_REG_SIZE(DT_CHOSEN(zephyr_sram))

/* Offset between IBUS and DBUS pointing to the same memory location*/
#define I_D_RAM_OFFSET (SOC_DIRAM_IRAM_LOW - SOC_DIRAM_DRAM_LOW)

static inline bool is_iram(uint32_t addr){
    return (addr >= SOC_IRAM_LOW && addr < SOC_IRAM_HIGH);
}

static inline bool is_dram(uint32_t addr){
    return (addr >= SOC_DRAM_LOW && addr < SOC_DRAM_HIGH);
}

int dt_esp32_loader_get_metadata(uint8_t slot_id, uint32_t padding_size, esp_image_load_header_t* metadata){
    const struct flash_area *slot;

    int err = flash_area_open(slot_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", slot_id);
        abort();
    }

    err = flash_area_read(slot, padding_size, metadata, sizeof(esp_image_load_header_t));

    if(err != 0){
        LOG_ERR("cannot read flash area - id %d: %d", slot_id, err);
        return err == -EINVAL ? -EINVAL : -EIO;
    }

    if(metadata->header_magic != ESP_LOAD_HEADER_MAGIC){
        return -ENOENT;
    }

    flash_area_close(slot);

    return 0;
}

int dt_esp32_loader_init(uint8_t slot_id, uint32_t padding_size, dt_esp32_boot_ctx_t* ctx) {
    int err = 0;

    ctx->before_boot_hook = NULL;
    ctx->valid = false;

    err = ~0;
    err = dt_esp32_loader_get_metadata(slot_id, padding_size, &(ctx->meta));
    if(err != 0){
        LOG_ERR("cannot get image metadata");
        return err;
    }

    const struct flash_area *slot;
    err = flash_area_open(slot_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", slot_id);
        abort();
    }
    
    ctx->fa_id = slot_id;
    ctx->fa_off = slot->fa_off;
    ctx->padding_size = padding_size;

    LOG_DBG("image at 0x%x: padding size: %d", ctx->fa_off, padding_size);

    flash_area_close(slot);

    ctx->valid = true;
    return 0;
}

int dt_esp32_loader_set_before_boot_hook(dt_esp32_boot_ctx_t* ctx, dt_esp32_loader_before_boot_hook_t hook, void* user){
    ctx->before_boot_hook = hook;
    ctx->before_boot_hook_user = user;
    return 0;
}

static inline bool is_overlapping(uint32_t x1, uint32_t x2, uint32_t y1, uint32_t y2){
    return x1 <= y2 && y1 <= x2;
}

static bool verifiy_segment(uint32_t addr, uint32_t size){
    uint32_t segment_start = addr;
    uint32_t segment_end = addr + size - 1;

    bool not_fit = false;
    
    if(is_iram(segment_start)){
        not_fit = segment_end >= SOC_IRAM_HIGH;
        segment_start -= I_D_RAM_OFFSET;
        segment_end   -= I_D_RAM_OFFSET;
    } else if(is_dram(segment_start)){
        not_fit = segment_end >= SOC_DRAM_HIGH;
    } else {
        LOG_ERR("not ram segment, cannot load");
        return false;
    }

    if(not_fit){
        LOG_ERR("end of the segment is not within ram address space");
        return false;
    }

    bool overlap = is_overlapping(
        IMAGE_RAM_ADDR, IMAGE_RAM_ADDR + IMAGE_RAM_SIZE - 1,
        segment_start, segment_end);

    if(overlap) {
        LOG_ERR("collision with currently running image");
    }

    return !overlap;
}

static int load_segment(dt_esp32_boot_ctx_t* ctx, uint32_t load_addr, uint32_t flash_off, uint32_t size) {
    if(load_addr == 0 || size == 0 || load_addr == ~0 || size == ~0){
        LOG_DBG("skipping segment");
        return 0;
    }

    if(!verifiy_segment(load_addr, size)){
        LOG_ERR("failed to load memory segment: addr: 0x%x size: %d", load_addr, size);
        return -EINVAL;
    }

    LOG_DBG(
        "loading segment: load addr: 0x%-8x  size: %-6d  offset: 0x%-8x",
        load_addr, size, flash_off
    );

    uint8_t* dst = (uint8_t*)load_addr;

    const struct flash_area *slot;
    int err = flash_area_open(ctx->fa_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", ctx->fa_id);
        abort();
    }
    
    err = flash_area_read(slot, flash_off, dst, size);

    if(err != 0){
        LOG_ERR("loading segment to memory failed: %d", err);
        return err == -EINVAL ? -EINVAL : -EIO;
    }

    flash_area_close(slot);

    return 0;
}

int dt_esp32_loader_load_segments(dt_esp32_boot_ctx_t* ctx) {
    int err;

    if(!ctx->valid) {
        return -EINVAL;
    }

    LOG_INF("loading memory segments");

    err = ~0;
    err = load_segment(ctx, 
        ctx->meta.iram_dest_addr,
        ctx->meta.iram_flash_offset,
        ctx->meta.iram_size
    );

    if(err != 0){
        LOG_ERR("loading iram segment failed");
        goto err;
    }

    err = ~0;
    err = load_segment(ctx, 
        ctx->meta.dram_dest_addr,
        ctx->meta.dram_flash_offset,
        ctx->meta.dram_size
    );

    if(err != 0){
        LOG_ERR("loading dram segment failed");
        goto err;
    }

    if(ctx->meta.entry_addr == 0 || ctx->meta.entry_addr == ~0){
        err = -EINVAL;
        goto err;
    }

    return 0;

err:
    ctx->valid = false;
    return err;
}

void dt_esp32_loader_boot(dt_esp32_boot_ctx_t* ctx){
    LOG_INF("booting new image");
    
    if(!ctx->valid){
        return;
    }

    if(ctx->meta.entry_addr == 0 || ctx->meta.entry_addr == ~0){
        LOG_ERR("invalid entry address");
        ets_printf("invalid address\n");
        return;
    }


    if(ctx->before_boot_hook != NULL){
        LOG_DBG("calling before_boot_hook");
        if(!ctx->before_boot_hook(ctx, ctx->before_boot_hook_user)){
            LOG_DBG("aborting boot");
            return;
        }
    }

    LOG_INF("entry address: 0x%x", ctx->meta.entry_addr);

    esp_rom_uart_tx_wait_idle(0);

    irq_lock();

    ((void (*)(void))ctx->meta.entry_addr)();

    abort();
}