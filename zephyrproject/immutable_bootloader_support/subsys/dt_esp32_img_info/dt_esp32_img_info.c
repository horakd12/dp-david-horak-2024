#include <assert.h>
#include <stdlib.h>
#include <zephyr/kernel.h>
#include <zephyr/init.h>
#include <zephyr/logging/log.h>
#include <zephyr/storage/flash_map.h>
#include <dronetag/dt_esp32_img_info.h>

LOG_MODULE_REGISTER(dt_esp32_img_info, CONFIG_DT_ESP32_IMG_INFO_LOG_LEVEL);

#ifdef CONFIG_DT_ESP32_IMG_INFO

#include <zephyr/toolchain/common.h>

#if CONFIG_BOOTLOADER_IMMUTABLE_ESP32_S1_VARIANT
    #define IMAGE_FLASH_ADDR    FIXED_PARTITION_OFFSET(s1)
#elif CONFIG_BOOTLOADER_IMMUTABLE_ESP32
    #define IMAGE_FLASH_ADDR    FIXED_PARTITION_OFFSET(s0)
#elif CONFIG_MCUBOOT && !CONFIG_BOOTLOADER_IMMUTABLE_ESP32
    #define IMAGE_FLASH_ADDR    FIXED_PARTITION_OFFSET(boot_partition)
#else
    #define IMAGE_FLASH_ADDR FIXED_PARTITION_OFFSET(slot0_partition)
#endif

#define ESP32_IMG_INFO_ATTR __attribute__((__section__(".esp32_img_info"), __used__))

extern const uint32_t _image_size[];

static const ESP32_IMG_INFO_ATTR dt_esp32_img_info_t esp32_img_info = {
    .magic          = DT_ESP32_IMG_INFO_MAGIC,
    .version        = 1,
    .size           = sizeof(dt_esp32_img_info_t),
    .img_version    = CONFIG_DT_ESP32_IMG_INFO_VERSION,
    .img_size       = (uint32_t)_image_size,
    .img_flash_addr = IMAGE_FLASH_ADDR,
    .valid          = DT_ESP32_IMG_INFO_VALID_VAL,
};

_Static_assert(sizeof(CONFIG_DT_ESP32_IMG_INFO_VERSION) <= sizeof(esp32_img_info.img_version), "ESP32_IMG_INFO_VERSION is longer than version field in structure");
_Static_assert(sizeof(esp32_img_info) == 128, "Size of esp32_img_info is not 128 bytes");

#endif

#define ESP32_IMAGE_METADATA_SIZE 0x20

int dt_esp32_img_info_get(uint8_t slot_id, uint32_t padding_size, dt_esp32_img_info_t* img_info){
    int err = 0;

    const struct flash_area *slot;

    err = flash_area_open(slot_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", slot_id);
        abort();
    }

    dt_esp32_img_info_t info;
    err = flash_area_read(slot, padding_size + ESP32_IMAGE_METADATA_SIZE, &info, sizeof(dt_esp32_img_info_t));
    if(err != 0){
        err = err == -EINVAL ? -ENOENT : -EIO;
        goto end;
    }

    
    if(info.magic != DT_ESP32_IMG_INFO_MAGIC){
        err = -ENOENT;
        goto end;
    }
    
    memcpy(img_info, &info, sizeof(info));
end:
    flash_area_close(slot);
    return err;
}

int dt_esp32_img_info_invalidate(uint8_t slot_id, uint32_t padding_size, dt_esp32_img_info_t* img_info){
    int err = 0;
    LOG_DBG("invalidating image");

    dt_esp32_img_info_t img_inf;
    err = dt_esp32_img_info_get(slot_id, padding_size, &img_inf);
    if(err != 0){
        return err;
    }

    const struct flash_area *slot;

    err = flash_area_open(slot_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", slot_id);
        abort();
    }

    if(img_inf.valid != DT_ESP32_IMG_INFO_VALID_VAL){
        LOG_DBG("image at 0x%lx is already invalidated", slot->fa_off);
        return 0;
    }
    
    uint32_t invalid_val = DT_ESP32_IMG_INFO_INVALID_VAL;

    err = flash_area_write(slot,
            padding_size + ESP32_IMAGE_METADATA_SIZE + offsetof(dt_esp32_img_info_t, valid), 
            &invalid_val, sizeof(invalid_val));

    if(err != 0){
        LOG_ERR("failed to write invalid value to img_info");
        return err;
    }

    if(img_info != NULL){
        img_info->valid = invalid_val;
    }

    LOG_INF("image at 0x%lx invalidated", slot->fa_off);

    flash_area_close(slot);

    return err;
}