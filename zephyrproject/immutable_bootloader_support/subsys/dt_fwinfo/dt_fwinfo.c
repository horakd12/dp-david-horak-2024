#include<stdint.h>
#include<errno.h>
#include <string.h>
#include<zephyr/storage/flash_map.h>
#include <dronetag/dt_esp32_img_info.h>
#include <zephyr/mgmt/mcumgr/grp/img_mgmt/img_mgmt.h>

typedef struct mcuboot_status {
    size_t version;
    bool valid;
} mcuboot_status_t;

static int dt_fwinfo_read_mcuboot_status(uint8_t fa_id, mcuboot_status_t* status){
    dt_esp32_img_info_t img_info;

    memset(status, 0, sizeof(mcuboot_status_t));

    int res = dt_esp32_img_info_get(fa_id, 0x20, &img_info);
    if(res != 0){
        status->valid = false;
        return res == -ENOENT ? 0 : res;
    }

    status->version = img_info.img_version;
    status->valid = img_info.valid == DT_ESP32_IMG_INFO_VALID_VAL;

    return res;
}

uint8_t app_hash[32];
int mcuboot_version = 0;
int mcuboot_slot = 0;

int dt_fwinfo_init(){
    int res = 0;

    bool s0_active;
    uint32_t version;

    struct mcuboot_status s0 = {0};
    struct mcuboot_status s1 = {0};

    res = dt_fwinfo_read_mcuboot_status(FIXED_PARTITION_ID(s0), &s0);
    if(res != 0){
        return -EFAULT;
    }

    res = dt_fwinfo_read_mcuboot_status(FIXED_PARTITION_ID(s1), &s1);
    if(res != 0){
        return -EFAULT;
    }

    if(!s0.valid && !s1.valid){
        return -EFAULT;
    }

    if (!s1.valid) {
        /* No s1 found or not valid, s0 is active */
        s0_active = true;
    } else if(!s0.valid) {
        /* No s0 found or not valid, s1 is active */
        s0_active = false;
    } else {
        /* Both s0 and s1 found, check who is active */
        s0_active = s0.version >= s1.version;
    }
    version = s0.version;
    if(!s0_active) {
        version = s1.version;
    }

    mcuboot_version = version;
    mcuboot_slot = s0_active ? 0 : 1;

#ifdef CONFIG_MCUMGR_GRP_IMG
    struct image_version img_ver;
    uint32_t flags;
    img_mgmt_read_info(0, &img_ver, app_hash, &flags);
#endif

    return 0;
}

#ifdef CONFIG_SHELL
#include <zephyr/shell/shell.h>

static int cmd_dt_fwinfo(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    char hash[65] = {0};

    for(int i = 0; i < 32; i++){
        sprintf(&hash[i*2], "%02x", app_hash[i]);
    }

    shell_print(shell, "firmware hash: %s", hash);

    shell_print(shell, "mcuboot version: %d", mcuboot_version);
    shell_print(shell, "mcuboot slot: %d",mcuboot_slot);
    
    return 0;
}

SHELL_CMD_REGISTER(dt_fwinfo, NULL, "Show firmware info", cmd_dt_fwinfo);
#endif

SYS_INIT(dt_fwinfo_init, APPLICATION, 0);