/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <stdlib.h>
#include <string.h>
#include "bootloader_flash_priv.h"
#include <errno.h>
#include "esp_app_format.h"
#include "esp_rom_sys.h"
#include "reset_reasons.h"
#include "bootloader_sha.h"
#include <zephyr/storage/flash_map.h>
#include <zephyr/logging/log.h>

#include "soc/extmem_reg.h"
#include "soc/cache_memory.h"
#include "esp_efuse.h"
#include "esp_efuse_table.h"
#include "secure_boot.h"
#include "esp_rom_crc.h"

#include <dronetag/dt_esp32_img_info.h>
#include <dronetag/boot/dt_esp32_loader.h>
#include <dronetag/boot/dt_esp32_secure_boot.h>

LOG_MODULE_REGISTER(boot, CONFIG_BOOT_IMMUTABLE_ESP32_LOG_LEVEL);

#define BOOT_SLOT_0 FIXED_PARTITION_ID(s0)
#define BOOT_SLOT_1 FIXED_PARTITION_ID(s1)

#define IMAGE_PAD_SIZE 0x20

static void verify_and_boot(uint8_t slot_id, uint32_t padding, dt_esp32_img_info_t* img_info){
    int err;

    const struct flash_area* slot;

    err = ~0;
    err = flash_area_open(slot_id, &slot);
    if(err != 0){
        LOG_ERR("cannot open flash area %d, aborting", slot_id);
        abort();
    }

    LOG_INF("attemping to boot image at 0x%lx", slot->fa_off);

    flash_area_close(slot);

    LOG_DBG("img_info.valid: 0x%x", img_info->valid);

    if(img_info->valid != DT_ESP32_IMG_INFO_VALID_VAL){
        goto invalidate;
    }

    static dt_esp32_boot_ctx_t ctx;
    memset(&ctx, 0, sizeof(ctx));

    err = ~0;
    err = dt_esp32_loader_init(slot_id, IMAGE_PAD_SIZE, &ctx);
    if(err != 0){
        goto invalidate;
    }

    err = ~0;
    err = dt_esp32_secure_boot_verify_image(slot_id, padding, img_info->img_size);
    if(err != 0){
        LOG_ERR("image verification failed");
        goto invalidate;
    }

    err = ~0;
    err = dt_esp32_loader_load_segments(&ctx);
    if(err != 0){
        LOG_ERR("cannot load segments to memory: %d", err);
        goto invalidate;
    }

    dt_esp32_loader_boot(&ctx);

invalidate:
    if(err == -EIO){
        // This should normally never happen.
        // There was error when reading/writing to flash.
        // In the flash there can still be valid image.
        // We're aborting execution to avoid invalidating valid image
        abort();
    }

    err = ~0;
    err = dt_esp32_img_info_invalidate(slot_id, padding, img_info);
    if(err == -EIO) {
        abort();
    }
    if(err != 0){
        LOG_WRN("cannot invalidate image");
        return;
    }

    img_info->valid = DT_ESP32_IMG_INFO_INVALID_VAL;

    LOG_INF("image is not valid");

    return;
}

int main(void)
{
    int err = ~0;

    static dt_esp32_img_info_t s0_info = {0};
    static dt_esp32_img_info_t s1_info = {0};

    err = ~0;
    err = dt_esp32_img_info_get(BOOT_SLOT_0, IMAGE_PAD_SIZE, &s0_info);
    if(err == -EIO) abort();

    err = ~0;
    err = dt_esp32_img_info_get(BOOT_SLOT_1, IMAGE_PAD_SIZE, &s1_info);
    if(err == -EIO) abort();

    bool s0_valid = s0_info.valid == DT_ESP32_IMG_INFO_VALID_VAL;
    bool s1_valid = s1_info.valid == DT_ESP32_IMG_INFO_VALID_VAL;

    LOG_INF("slot 0: 0x%x, valid: %d, version: %d", FIXED_PARTITION_OFFSET(s0), s0_valid, s0_info.img_version);
    LOG_INF("slot 1: 0x%x, valid: %d, version: %d", FIXED_PARTITION_OFFSET(s1), s1_valid, s1_info.img_version);

    if(!s1_valid || (s0_info.img_version >= s1_info.img_version)) {
        verify_and_boot(BOOT_SLOT_0, IMAGE_PAD_SIZE, &s0_info);
        verify_and_boot(BOOT_SLOT_1, IMAGE_PAD_SIZE, &s1_info);
    }
    else {
        verify_and_boot(BOOT_SLOT_1, IMAGE_PAD_SIZE, &s1_info);
        verify_and_boot(BOOT_SLOT_0, IMAGE_PAD_SIZE, &s0_info);
    }
    
    LOG_ERR("no valid image found");

    while(1) {};

    return 0;
}
