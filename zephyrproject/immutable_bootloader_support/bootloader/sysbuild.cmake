if(SB_CONFIG_BOOTLOADER_IMMUTABLE_ESP32)
    ExternalZephyrProject_Add(
        APPLICATION immutable_esp32
        SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR}/immutable_esp32/
    )
endif()

if(SB_CONFIG_ESP32_BUILD_MCUBOOT_S1)
    ExternalZephyrProject_Add(
        APPLICATION mcuboot_s1
        SOURCE_DIR ${ZEPHYR_MCUBOOT_MODULE_DIR}/boot/zephyr
    )

    ## Share config with mcuboot image

    get_cmake_property(sysbuild_cache CACHE_VARIABLES)
    foreach(var_name ${sysbuild_cache})
      if("${var_name}" MATCHES "^(mcuboot_.*)$")
        string(LENGTH mcuboot tmplen)
        string(SUBSTRING "${var_name}" ${tmplen} -1 tmp)
        set(mcuboot_s1${tmp} "${${var_name}}" CACHE UNINITIALIZED "" FORCE)
      endif()
    endforeach()
  
    ExternalProject_Get_Property(mcuboot BINARY_DIR)
    set(mcuboot_BINARY_DIR ${BINARY_DIR})

    ExternalProject_Get_Property(mcuboot_s1 BINARY_DIR)
    set(mcuboot_s1_BINARY_DIR ${BINARY_DIR})

    ExternalProject_Add_Step(mcuboot_s1 variant_config
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
              ${mcuboot_BINARY_DIR}/zephyr/.config
              ${mcuboot_s1_BINARY_DIR}/zephyr/.config
      DEPENDERS build
      ALWAYS True
    )
  
    # Disable menuconfig and friends in variant builds by substitution with a
    # dummy target that does nothing except returning successfully.
    set_property(TARGET mcuboot_s1 APPEND PROPERTY _EP_CMAKE_ARGS
      -DKCONFIG_TARGETS=variant_config
      "-DEXTRA_KCONFIG_TARGET_COMMAND_FOR_variant_config=-c\;''"
    )

    set_config_bool(mcuboot_s1 CONFIG_BOOTLOADER_IMMUTABLE_ESP32 y)
    set_config_bool(mcuboot_s1 CONFIG_BOOTLOADER_IMMUTABLE_ESP32_S1_VARIANT y)
endif()