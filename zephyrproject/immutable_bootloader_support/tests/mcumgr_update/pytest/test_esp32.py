import logging
from common import *

import pytest
import time

from twister_harness.device.hardware_adapter import HardwareAdapter
from twister_harness import DeviceAdapter, Shell, MCUmgr
from twister_harness.device.factory import DeviceFactory
from twister_harness.twister_harness_config import DeviceConfig, TwisterHarnessConfig

from dataclasses import dataclass

from pathlib import Path

import sys

sys.path.append(f'{ZEPHYR_BASE}/../immutable_bootloader_support/scripts/pylib')

from dt_lib.mcumgr_upgrade_test import *
from dt_lib.dt_fw_builder import *

logger = logging.getLogger(__name__)

@pytest.fixture(scope='function')
def fw_builder(dt_dut: DTDeviceAdapter) -> DTFirmwareBuilder:
    board = Path(dt_dut.device_config.build_dir).parent.name

    fw_builder = DTESP32FirmwareBuilder(APP_DIR, board)
    fw_builder.add_cmake_arg("-DSB_CONF_FILE=sysbuild_esp32.conf")
    fw_builder.add_west_arg("--sysbuild")

    return fw_builder


invalid_immutable_key = './immutable_bootloader_support/tests/mcumgr_update/keys/esp32/immutable_key_wrong.pem'


@pytest.mark.parametrize("test_config", mcuboot_update_test_default_config)
def test_upgrade_bootloader(dt_dut: DTDeviceAdapter, fw_builder: DTFirmwareBuilder,
                            test_config: MCUbootUpdateTestParam, fwinfo_shell, mcumgr):
    test = ESP32MCUbootUpdateTest(test_config, mcumgr, fw_builder,
                                  fwinfo_shell, dt_dut.device_config.build_dir)

    test.set_invalid_immutable_key(invalid_immutable_key)
    test.run_test()


invalid_mcuboot_key = './immutable_bootloader_support/tests/mcumgr_update/keys/mcuboot_wrong.pem'


@pytest.mark.parametrize("test_config", app_update_test_default_config)
def test_upgrade_app(dt_dut: DTDeviceAdapter, fw_builder: DTFirmwareBuilder,
                     test_config: MCUbootUpdateTestParam, fwinfo_shell, mcumgr):
    test = AppUpdateTest(test_config, mcumgr, fw_builder,
                         fwinfo_shell, dt_dut.device_config.build_dir)
    test.set_invalid_signature_key(invalid_mcuboot_key)
    test.run_test()
