import logging

import pytest
import time

from twister_harness.device.hardware_adapter import HardwareAdapter
from twister_harness import DeviceAdapter, Shell, MCUmgr
from twister_harness.device.factory import DeviceFactory
from twister_harness.twister_harness_config import DeviceConfig, TwisterHarnessConfig
from typing import Generator

import serial
import re

import subprocess
import shlex
from pathlib import Path
import os

import sys

ZEPHYR_BASE = os.getenv("ZEPHYR_BASE")
APP_DIR = Path(f'{ZEPHYR_BASE}/../immutable_bootloader_support/tests/mcumgr_update')

sys.path.append(f'{ZEPHYR_BASE}/../immutable_bootloader_support/scripts/pylib')

from dt_lib.dt_fw_builder import DTFirmwareBuilder
from dt_lib.dt_fwinfo_shell import DTFWInfoShell, DTFWInfo, connect_serial, ZephyrShell

logger = logging.getLogger(__name__)



class DTDeviceAdapter(HardwareAdapter):
    def launch(self) -> None:
        """
        Start by closing previously running application (no effect if not
        needed). Then, flash and run test application. Finally, start a reader
        thread capturing an output from a device.
        """
        self.close()
        self._clear_internal_resources()

        if not self.command:
            self.generate_command()
        self._flash_and_run()
        self._device_run.set()


@pytest.fixture(scope='function')
def mcumgr(dt_dut: DTDeviceAdapter) -> MCUmgr:
    serial = dt_dut.device_config.serial
    baud_rate = dt_dut.device_config.baud

    mcumgr_conn_string = f"dev={serial},baud={baud_rate},mtu=256"

    return MCUmgr.create_for_serial(mcumgr_conn_string)

@pytest.fixture(scope='function')
def fwinfo_shell(dt_dut: DTDeviceAdapter) -> DTFirmwareBuilder:
    serial = dt_dut.device_config.serial
    baud_rate = dt_dut.device_config.baud

    shell = ZephyrShell(
        port=serial,
        ser_fac=lambda port: connect_serial(
            device=port, baudrate=baud_rate, timeout=1
        ),
    )

    shell.close()

    return DTFWInfoShell(shell)


@pytest.fixture(scope='session')
def twister_harness_config(request: pytest.FixtureRequest) -> TwisterHarnessConfig:
    """Return twister_harness_config object."""
    twister_harness_config: TwisterHarnessConfig = request.config.twister_harness_config  # type: ignore
    return twister_harness_config


@pytest.fixture(scope='session')
def dt_device_object(twister_harness_config: TwisterHarnessConfig) -> Generator[DTDeviceAdapter, None, None]:
    """Return device object - without run application."""
    device_config: DeviceConfig = twister_harness_config.devices[0]
    device_object = DTDeviceAdapter(device_config)
    try:
        yield device_object
    finally:  # to make sure we close all running processes execution
        device_object.close()

@pytest.fixture(scope='function')
def dt_dut(request: pytest.FixtureRequest, dt_device_object: DTDeviceAdapter) -> Generator[DTDeviceAdapter, None, None]:
    """Return launched device - with run application."""
    test_name = request.node.name
    dt_device_object.initialize_log_files(test_name)
    try:
        yield dt_device_object
    finally:  # to make sure we close all running processes execution
        dt_device_object.close()
