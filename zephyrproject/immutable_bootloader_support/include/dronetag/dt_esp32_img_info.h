#ifndef DT_ESP32_IMG_INFO_H
#define DT_ESP32_IMG_INFO_H

#include <stdint.h>
#include <zephyr/toolchain/gcc.h>

#define DT_ESP32_IMG_INFO_MAGIC         0x9DA580F7

#define DT_ESP32_IMG_INFO_VALID_VAL     0xA4C2FF75
#define DT_ESP32_IMG_INFO_INVALID_VAL   0xFFFF0000

/** 
 * @brief Image Info structure
 * 
 * This structure is used as image description
 */
typedef struct {
    /** @brief magic word */
    uint32_t magic;
    /** @brief version of image info structure */
    uint16_t version;
    /** @brief size of this structure */
    uint16_t size;
    /** @brief image version */
    uint32_t img_version;
    /** @brief image size (incuding this structure and metadata) */
    uint32_t img_size;
    /** @brief address of the image in flash */
    uint32_t img_flash_addr;
    /** @brief valid flag */
    uint32_t valid;

    uint8_t reserved[104];
} __packed dt_esp32_img_info_t;

/** 
 * @brief Get image info 
 * 
 * This function finds image info in specified slot
 * 
 * @param slot_id       id of flash area
 * @param padding_size  padding size, beginning of the image relative to the slot
 * @param img_info      pointer to dt_esp32_img_info_t struct
 *
 * @retval 0	        If successful, negative errno code otherwise.
*/
int dt_esp32_img_info_get(uint8_t slot_id, uint32_t padding_size, dt_esp32_img_info_t* img_info);

/** 
 * @brief Invalidate image
 * 
 * This function invalidates image
 * DT_ESP32_IMG_INFO_INVALID_VAL will be written to the memory
 * 
 * @param slot_id       id of flash area
 * @param padding_size  padding size, beginning of the image relative to the slot
 * @param img_info      pointer to dt_esp32_img_info_t struct
 *
 * @retval 0	        If successful, negative errno code otherwise.
*/
int dt_esp32_img_info_invalidate(uint8_t slot_id, uint32_t padding_size, dt_esp32_img_info_t* img_info);

#endif /* DT_ESP32_IMG_INFO_H */