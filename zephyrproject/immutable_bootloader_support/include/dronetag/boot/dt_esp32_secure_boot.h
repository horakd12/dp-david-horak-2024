#ifndef DT_ESP32_SECURE_BOOT_H
#define DT_ESP32_SECURE_BOOT_H

#include <stdint.h>

/** 
 * @brief Verify image signature
 * 
 * This function verifies image signature.
 * 
 * Verification is done in folowing steps:
 *  1) Checks if SECURE_BOOT_EN eFuse bit is set, 
 *     verification is skipped otherwise.
 *  2) Check if signature block is valid (check magic and crc).
 *  3) Calculate SHA256 hash of public key and verify
 *     calculated hash against key stored in eFuse.
 *  4) Calculate SHA256 hash of the image.
 *  5) Verify signature of the calculated hash.
 * 
 * @param fa_id         flash area id
 * @param offset        beginning of the image relative to the flash area
 * @param size          size of the image
 *
 * @retval 0	        If successful, negative errno code otherwise.
*/
int dt_esp32_secure_boot_verify_image(uint8_t fa_id, uint32_t offset, uint32_t size);

#endif /* DT_ESP32_SECURE_BOOT_H */