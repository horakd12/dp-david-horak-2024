#ifndef DT_BOOT_ESP32_LOADER_H
#define DT_BOOT_ESP32_LOADER_H

#include <stdint.h>
#include <zephyr/storage/flash_map.h>

#include "boot/esp_mcuboot_image.h"


typedef struct dt_esp32_boot_ctx dt_esp32_boot_ctx_t;

typedef bool (*dt_esp32_loader_before_boot_hook_t)(dt_esp32_boot_ctx_t* ctx, void* user);

/**
 * @brief Structure containing data used in boot process
 */
struct dt_esp32_boot_ctx {
    /** @brief flash area id */
    uint8_t fa_id;
    /** @brief address of the flash area */
    uint32_t fa_off;
    /** @brief begining of the image relative to the flash area */
    uint32_t padding_size;
    /** @brief image metadata */
    esp_image_load_header_t meta;
    /** @brief user data passed to before_boot_hook*/
    void* before_boot_hook_user;
    /** @brief before_boot_hook function */
    dt_esp32_loader_before_boot_hook_t before_boot_hook;
    /** @brief valid flag */
    bool valid;
};

/** 
 * @brief Load image metadata
 * 
 * This function checks flash area for image existance
 * and loads metadata
 * 
 * @param slot_id       flash area id
 * @param padding_size  beginning of the image relative to 
 *                      the flash area (excluding mcuboot header)
 * @param metadata      parsed metadata
 * 
 * @retval 0            If successful, negative errno code otherwise.
 */
int dt_esp32_loader_get_metadata(uint8_t slot_id, uint32_t padding_size, 
                                 esp_image_load_header_t* metadata);

/** 
 * @brief Set before boot hook
 * 
 * This function sets before_boot_hook function.
 * Function will be called right before jumping to the new image
 * 
 * @param ctx           loader context
 * @param hook          function to be set
 * @param user          user data that is passed when hook is called
 * 
 * @retval 0            If successful, negative errno code otherwise.
 */
int dt_esp32_loader_set_before_boot_hook(dt_esp32_boot_ctx_t* ctx, 
                                         dt_esp32_loader_before_boot_hook_t hook, 
                                         void* user);

/** 
 * @brief Initialize loader context
 * 
 * This function checks flash area for image existance
 * and initializes loader context
 * 
 * @param slot_id       flash area id
 * @param padding_size  beginning of the image relative to 
 *                      the flash area (excluding mcuboot header)
 * @param ctx           loader context
 * 
 * @retval 0            If successful, negative errno code otherwise.
 */
int dt_esp32_loader_init(uint8_t slot_id, uint32_t padding_size, dt_esp32_boot_ctx_t* ctx);

/** 
 * @brief Load new image to the memory
 * 
 * This function will load image's memory segments into memory.
 * Memory segments is located in metadata.
 * Function checks for collision with currently running image.
 * 
 * @param ctx   loader context
 * 
 * @retval 0    If successful, negative errno code otherwise.
 * 
 */
int dt_esp32_loader_load_segments(dt_esp32_boot_ctx_t* ctx);

/** 
 * @brief Jump to the new image
 * 
 * This function jumps to the new image. 
 * Destination adress is located in metadata.
 * 
 * This function usually never returns.
 * 
 * @param ctx   loader context
 */
void dt_esp32_loader_boot(dt_esp32_boot_ctx_t* ctx);

#endif /* DT_BOOT_ESP32_LOADER_H */