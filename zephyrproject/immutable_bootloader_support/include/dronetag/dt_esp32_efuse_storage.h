#ifndef DT_ESP32_EFUSE_STORAGE_H
#define DT_ESP32_EFUSE_STORAGE_H

#include <stdint.h>

#include "esp_efuse.h"

/** 
 * @brief Reads bits from eFUSE field and writes it into an array.
 * 
 * This function will read bits in eFuse and write them into array
 * This API is compatibile with API in hal_espressif
 * 
 * @param[in]  field            A pointer to the structure describing the fields of efuse.
 * @param[out] data             A pointer to array that will contain the result of reading.
 * @param[in]  data_size_bits   The number of bits required to read.
 *
 * @retval 0    If successful, error code otherwise.
 */
int dt_esp32_efuse_read_blob(const esp_efuse_desc_t* field[], void* data, int data_size_bits);

/** 
 * @brief Writes bits to eFUSE based on array content.
 * 
 * This function will write content of given array into the eFuse
 * This API is compatibile with API in hal_espressif
 * 
 * @param[in]  field            A pointer to the structure describing the fields of efuse.
 * @param[out] data             A pointer to array that contain data to be written to eFuse.
 * @param[in]  data_size_bits   The number of bits required to read.
 *
 * @retval 0    If successful, error code otherwise.
 */
int dt_esp32_efuse_write_blob(const esp_efuse_desc_t* field[], const void* data, int data_size_bits);

#define DT_ESP32_MAX_IMMUTABLE_KEY_NUM 3

/** 
 * @brief Structure for holding secure boot key digests
 */
typedef struct {
    /** @brief key digests */
    uint8_t key[DT_ESP32_MAX_IMMUTABLE_KEY_NUM][32];
    /** @brief number of key digests */
    int key_num;
} dt_esp32_efuse_keys_t;

/** 
 * @brief Get key digests used by immutable bootloader
 * 
 * This function will return key digests use by immutable bootloader
 * Keys will be retreived from eFuse storage
 * See DT_ESP32_EFUSE_STORAGE_IMMUTABLE_KEYx_BLOCK option for setting block number for key x
 * 
 * @param keys  pointer to dt_esp32_efuse_keys_t struct
 *
 * @retval 0    If successful, negative errno code otherwise.
 */
int dt_esp32_efuse_get_immutable_keys(dt_esp32_efuse_keys_t* keys);

/** 
 * @brief Get content of user data area in eFuse
 * 
 * This function fills data array with user data content in eFuse
 * 
 * See DT_ESP32_EFUSE_STORAGE_USER_DATAx_BLOCK option
 * for setting location of user data area in eFuse
 * 
 * @param data          pointer to array
 * @param size_bytes    size of array in bytes
 *
 * @retval 0            If successful, negative errno code otherwise.
 */
int dt_esp32_efuse_read_user_data(void* data, int size_bytes);

/** 
 * @brief Write data to user data area in eFuse
 * 
 * This function writes content of data 
 * array to user data area located in eFuse.
 * 
 * See DT_ESP32_EFUSE_STORAGE_USER_DATAx_BLOCK option
 * for setting location of user data area in eFuse
 * 
 * @param data          pointer to array containing data
 * @param size_bytes    size of array in bytes
 *
 * @retval 0            If successful, negative errno code otherwise.
 */
int dt_esp32_efuse_write_user_data(void* data, int size_bytes);

#endif /* DT_ESP32_EFUSE_STORAGE_H */
